#!/bin/bash

set -e

PROJECT=Serialization

SRCDIR=target/j2objc
UMBRELLA="$SRCDIR/$PROJECT-umbrella.h"
SUBMOD=${PROJECT,,}-objc

mkdir -p $SRCDIR $SRCDIR/api $SRCDIR/core
cp api/target/j2objc/* $SRCDIR/api
cp core/target/j2objc/* $SRCDIR/core

MVN_VERSION=$(mvn -q \
    -Dexec.executable="echo" \
    -Dexec.args='${project.version}' \
    --non-recursive \
    org.codehaus.mojo:exec-maven-plugin:1.5.0:exec)

# Generate umbrella
cat <<EOF > $UMBRELLA
#import <UIKit/UIKit.h>
#import "IOSArray.h"
#import "IOSClass.h"
#import "IOSMetadata.h"
#import "IOSObjectArray.h"
#import "IOSPrimitiveArray.h"
#import "J2ObjC_common.h"
#import "J2ObjC_header.h"
#import "J2ObjC_source.h"
#import "J2ObjC_types.h"
#import "JavaObject.h"
#import "JreEmulation.h"
#import "JreRetainedWith.h"
#import "NSCopying+JavaCloneable.h"
#import "NSDataInputStream.h"
#import "NSDataOutputStream.h"
#import "NSDictionaryMap.h"
#import "NSException+JavaThrowable.h"
#import "NSNumber+JavaNumber.h"
#import "NSObject+JavaObject.h"
#import "NSString+JavaString.h"
EOF
find $SRCDIR -type f -regex '.*/[a-zA-Z0-9]+\.h' -printf "%f\n" | awk '{print "#import \""$1 "\""}' | sort | uniq >> $UMBRELLA
# echo '// Dependent Classes' >> $UMBRELLA
# find . -name '*.h' -or -name '*.m' -exec grep '#include' {} \; | sort | uniq | grep '/' --color=never >> $UMBRELLA
echo "Wrote $UMBRELLA"

# Update submodule repo
git submodule update --remote $SUBMOD
git submodule foreach git clean -f
git submodule foreach git checkout master
cp -rf $SRCDIR/* $SUBMOD
cp $UMBRELLA $SUBMOD
cd $SUBMOD

if [ -n "$(git status --porcelain)" ]; then
    git add .
    git commit -m "Update for $MVN_VERSION"
    git push origin master
fi

# if [ -z "$(git tag -l \$MVN_VERSION)" ]; then
#     git tag $MVN_VERSION
#     git push --tags origin master
# fi

cd ..
