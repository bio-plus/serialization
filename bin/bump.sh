#!/bin/bash

set -e

mvn versions:set -q -DnewVersion=$1
mvn versions:commit -q
find . -type f -name 'pom.xml' -exec git add {} \;
git commit -m "Release $1"
