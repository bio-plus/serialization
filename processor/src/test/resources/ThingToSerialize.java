package plus.bio.ser;

import plus.bio.ser.serializers.LongSerializer;

@plus.bio.ser.anno.AutoSerializer
public class ThingToSerialize {

  @plus.bio.ser.anno.SerializedField(serializer=LongSerializer.class)
  long aLong;

  public long getALong() {
    return aLong;
  }

  public void setALong(long v) {
    aLong = v;
  }

}
