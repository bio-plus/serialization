package plus.bio.ser.proc;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.Deserializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class CustomConstructorFieldsBad {

  @SerializedField
  final long longOne;

  @SerializedField
  long longTwo;

  @Deserializer
  public CustomConstructorFieldsBad(long one) {
    longOne = one;
  }

  public long getLongOne() {
    return longOne;
  }

  public long getLongTwo() {
    return longTwo;
  }

  public void setLongTwo(long v) {
    longTwo = v;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }
    final CustomConstructorFieldsBad other = (CustomConstructorFieldsBad) obj;
    return other.getLongOne() == getLongOne() && other.getLongTwo() == getLongTwo();
  }

}
