package plus.bio.ser;

@plus.bio.ser.anno.AutoSerializer
public class ThingWithList {

  @plus.bio.ser.anno.SerializedField
  List<Long> longs;

  public List<Long> getLongs() {
    return longs;
  }

  public void setLongs(List<Long> v) {
    longs = v;
  }

}
