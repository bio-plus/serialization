package plus.bio.ser.proc;

import java.util.Objects;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class ThreeField {

  @SerializedField
  int one;

  @SerializedField
  long two;

  @SerializedField
  String three;

  public int getOne() {
    return one;
  }

  public void setOne(int v) {
    one = v;
  }

  public long getTwo() {
    return two;
  }

  public void setTwo(long v) {
    two = v;
  }

  public String getThree() {
    return three;
  }

  public void setThree(String v) {
    three = v;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }
    final ThreeField other = (ThreeField) obj;
    return Objects.equals(getOne(), other.getOne()) && Objects.equals(getTwo(), other.getTwo())
        && Objects.equals(getThree(), other.getThree());
  }

}
