package plus.bio.ser.proc;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class OneField {

  @SerializedField
  long aLong;

  public long getALong() {
    return aLong;
  }

  public void setALong(long v) {
    aLong = v;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }
    final OneField other = (OneField) obj;
    return other.getALong() == getALong();
  }

}
