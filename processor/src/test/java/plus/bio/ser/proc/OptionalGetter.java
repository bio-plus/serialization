package plus.bio.ser.proc;

import javax.annotation.Nullable;

import com.google.common.base.Optional;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class OptionalGetter {

  @SerializedField
  @Nullable
  String nullable;

  public Optional<String> getNullable() {
    return Optional.fromNullable(nullable);
  }

  public void setNullable(String v) {
    nullable = v;
  }

}
