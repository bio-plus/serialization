package plus.bio.ser.proc;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class TwoSameField {

  @SerializedField
  long longOne;

  @SerializedField
  long longTwo;

  public long getLongOne() {
    return longOne;
  }

  public void setLongOne(long v) {
    longOne = v;
  }

  public long getLongTwo() {
    return longTwo;
  }

  public void setLongTwo(long v) {
    longTwo = v;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }
    final TwoSameField other = (TwoSameField) obj;
    return other.getLongOne() == getLongOne() && other.getLongTwo() == getLongTwo();
  }

}
