package plus.bio.ser.proc;

import javax.annotation.Nullable;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class NullableField {

  @SerializedField
  @Nullable
  String nullable;

  public String getNullable() {
    return nullable;
  }

  public void setNullable(String v) {
    nullable = v;
  }

}
