package plus.bio.ser.proc;

import java.util.Map;
import java.util.SortedSet;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.Deserializer;
import plus.bio.ser.anno.SerializedField;
import plus.bio.ser.provider.SortedSetProvider;

@AutoSerializer
public class ParameterizedFields {

  @SerializedField(collectionProvider=SortedSetProvider.class)
  SortedSet<Long> longs;

  @SerializedField
  Map<String, String> strings;

  @Deserializer
  public ParameterizedFields(SortedSet<Long> longs, Map<String, String> strings) {
    this.longs = longs;
    this.strings = strings;
  }

  public SortedSet<Long> getLongs() {
    return longs;
  }

  public Map<String, String> getStrings() {
    return strings;
  }

}
