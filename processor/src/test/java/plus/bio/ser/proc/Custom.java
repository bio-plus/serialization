package plus.bio.ser.proc;

import javax.naming.CompositeName;
import javax.naming.Name;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class Custom {

  @SerializedField(serializer = NameSerializer.class)
  CompositeName name;

  public Name getName() {
    return name;
  }

  public void setName(Name v) {
    name = CompositeName.class.cast(v);
  }
}
