package plus.bio.ser.proc;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class NestedField {

  @SerializedField
  OneField aField;

  public OneField getAField() {
    return aField;
  }

  public void setAField(OneField v) {
    aField = v;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }
    final NestedField other = (NestedField) obj;
    return other.getAField().equals(getAField());
  }

}
