package plus.bio.ser.proc;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class InheritedField extends OneField {

  @SerializedField
  long childField;

  public long getChildField() {
    return childField;
  }

  public void setChildField(long v) {
    childField = v;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }
    final InheritedField other = (InheritedField) obj;
    return super.equals(other) && childField == other.getChildField();
  }

}
