package plus.bio.ser.proc;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.Deserializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class CustomConstructor {

  @SerializedField
  private final long aLong;
  @SerializedField
  private final long bLong;

  @Deserializer
  public CustomConstructor(long aLong, long bLong) {
    this.aLong = aLong;
    this.bLong = bLong;
  }

  public long getALong() {
    return aLong;
  }
  
  public long getBLong() {
	return bLong;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }
    final CustomConstructor other = (CustomConstructor) obj;
    return other.getALong() == getALong() && other.getBLong() == getBLong();
  }

}
