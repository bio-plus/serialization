package plus.bio.ser.proc;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.SerializedField;

@AutoSerializer
public class UnknownField {

  @SerializedField
  Object uhoh;

  public Object getUhoh() {
    return uhoh;
  }

  public void setUhoh(Object v) {
    uhoh = v;
  }

}
