package plus.bio.ser.proc;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import javax.naming.CompositeName;
import javax.naming.Name;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class NameSerializer implements Serializer<Name> {

  @Override
  public int size(Registry r, Name o) throws MissingSerializerException {
    return 42;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Name o) throws IOException {
  }

  @Override
  public Name read(Registry r, DataInputStream in) throws IOException {
    return new CompositeName();
  }

}
