package plus.bio.ser.proc;

import static com.google.testing.compile.Compiler.javac;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import javax.naming.CompositeName;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.io.ByteStreams;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;

import plus.bio.ser.DefaultRegistry;
import plus.bio.ser.api.GeneratedSerializer;
import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;
import plus.bio.ser.api.VersionedSerializer;

public class GeneratedClassTest {

  static String pkgPrefix = "plus/bio/ser/proc/";
  static String classpathPrefix = "src/test/java/" + pkgPrefix;

  private Path outDir;
  private ByteArrayOutputStream outArray;
  private DataOutputStream out;
  private Registry r;

  @Before
  public void setUp() throws IOException {
    outDir = Files.createTempDirectory("ser");
    outDir.resolve(pkgPrefix).toFile().mkdirs();

    outArray = new ByteArrayOutputStream();
    out = new DataOutputStream(outArray);
    r = new DefaultRegistry();
  }

  @After
  public void tearDown() throws IOException {
    Files.walkFileTree(outDir, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        Files.delete(file);
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        Files.delete(dir);
        return FileVisitResult.CONTINUE;
      }
    });
  }

  @Test(expected = MissingSerializerException.class)
  public void testCustomSerializersAreNotRegistered() throws IOException {
    Registry r = new DefaultRegistry();
    GeneratedSerializer<Custom> ser = serializerInstance("Custom");
    ser.registerWith(r);

    r.serializer(CompositeName.class);
  }

  @Test
  public void testStaticRegistrationWithDefaultRegistryInstance() throws IOException {
    Registry r = DefaultRegistry.instance;
    serializerInstance("OneField");  // Trigger static registration

    Serializer<OneField> ser = r.serializer(OneField.class);

    assertThat(ser, not(nullValue()));
  }

  @Test
  public void testVersioning() throws IOException {
    Serializer<OneField> ser = serializerInstance("OneField");

    assertThat(ser, instanceOf(VersionedSerializer.class));

    assertThat(((VersionedSerializer)ser).version(), is(1));
  }

  @Test
  public void testSerializerWithOneField() throws IOException {
    Serializer<OneField> ser = serializerInstance("OneField");
    OneField val = new OneField();
    val.setALong(42);

    assertThat(ser.size(r, val), is(8));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(8));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testSerializerWithTwoOfTheSameField() throws IOException {
    Serializer<TwoSameField> ser = serializerInstance("TwoSameField");
    TwoSameField val = new TwoSameField();
    val.setLongOne(42);
    val.setLongTwo(24);

    assertThat(ser.size(r, val), is(16));

    ser.write(r, out, val);

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testSerializerWithThreeFields() throws IOException {
    Serializer<ThreeField> ser = serializerInstance("ThreeField");
    ThreeField val = new ThreeField();
    val.setOne(42);
    val.setTwo(24);
    val.setThree("hello world!");

    assertThat(ser.size(r, val), greaterThan(16));

    ser.write(r, out, val);

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testSerializerWithNestedField() throws IOException {
    Serializer<NestedField> ser = serializerInstance("OneField", "NestedField");
    NestedField val = new NestedField();
    OneField field = new OneField();
    field.setALong(42);
    val.setAField(field);

    assertThat(ser.size(r, val), greaterThan(8));

    ser.write(r, out, val);

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testSerializerWithInheritedField() throws IOException {
    Serializer<InheritedField> ser = serializerInstance("InheritedField");
    InheritedField val = new InheritedField();
    val.setALong(1);
    val.setChildField(2);

    assertThat(ser.size(r, val), is(16));

    ser.write(r, out, val);

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testSerializerWithCustomConstructor() throws IOException {
    Serializer<CustomConstructor> ser = serializerInstance("CustomConstructor");
    CustomConstructor val = new CustomConstructor(1, 2);

    ser.write(r, out, val);

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testSerializerParameterizedFields() throws IOException {
     Serializer<ParameterizedFields> ser = serializerInstance("ParameterizedFields");
     ParameterizedFields val = new ParameterizedFields(ImmutableSortedSet.of(42L), ImmutableMap.of("foo", "bar"));

     ser.write(r, out, val);
     ParameterizedFields got = ser.read(r, in());

     assertThat(got.getLongs(), instanceOf(TreeSet.class));
     assertThat(got.getLongs(), contains(42L));
     assertThat(got.getStrings(), instanceOf(HashMap.class));
     assertThat(got.getStrings(), hasEntry("foo", "bar"));
  }

  @Test
  public void testNullableFieldWithNullValue() throws IOException {
    Serializer<NullableField> ser = serializerInstance("NullableField");
    NullableField val = new NullableField();

    ser.write(r, out, val);
    NullableField got = ser.read(r, in());

    assertThat(got.getNullable(), nullValue());
  }

  @Test
  public void testOptionalGetterWithPresentValue() throws IOException {
    Serializer<OptionalGetter> ser = serializerInstance("OptionalGetter");
    OptionalGetter val = new OptionalGetter();
    val.setNullable("test");

    ser.write(r, out, val);
    OptionalGetter got = ser.read(r, in());

    assertThat(got.getNullable().isPresent(), is(true));
    assertThat(got.getNullable().get(), is("test"));
  }

  @Test(expected = MissingSerializerException.class)
  public void testMissingSerializer() throws IOException {
    Serializer<UnknownField> ser = serializerInstance("UnknownField");
    UnknownField val = new UnknownField();
    val.setUhoh(new Integer(42));

    ser.write(r, out, val);
  }

  private <T> GeneratedSerializer<T> serializerInstance(String... classes) throws MalformedURLException, FailedCompilation {
    return serializerInstance(false, classes);
  }

  @SuppressWarnings("unchecked")
  private <T> GeneratedSerializer<T> serializerInstance(boolean throwFailedCompilation, String... classes)
      throws MalformedURLException, FailedCompilation {
    final JavaFileObject[] fileObjs = new JavaFileObject[classes.length];
    final URLClassLoader loader = getClassLoader();

    try {
      for (int x = 0; x < classes.length; x++) {
        if (classes[x].endsWith(".java")) {
          fileObjs[x] = JavaFileObjects.forResource(classes[x]);
        } else {
          fileObjs[x] = JavaFileObjects.forResource(new File(classpathPrefix + classes[x] + ".java").toURI().toURL());
        }
      }
      Compilation compilation = javac().withProcessors(new AutoSerializerProcessor())
          .compile(fileObjs);

      if (compilation.errors().size() > 0 && throwFailedCompilation) {
        throw new FailedCompilation(compilation);
      }

      Class<? extends Serializer<?>> loadedClass = null;
      for (int x = 0; x < classes.length; x++) {
        writeClass(compilation, classes[x] + "Serializer");
      }
      for (int x = 0; x < classes.length; x++) {
        loadedClass = (Class<? extends Serializer<?>>) loader.loadClass("plus.bio.ser.proc." + classes[x] + "Serializer");
        // Only trigger registration when more than one class is loaded; this ensures tests that check static registration still test that
        if (x < classes.length-1) {
          register(loadedClass.newInstance(), classes[x]);
        }
      }
      return (GeneratedSerializer<T>) loadedClass.newInstance();
    } catch(FailedCompilation e) {
      throw e;
    } catch (IOException | ClassNotFoundException | InstantiationException
        | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }

  @SuppressWarnings("unchecked")
  private <T> void register(Serializer<T> s, String name) throws ClassNotFoundException, MalformedURLException {
    Class<T> c = (Class<T> )getClassLoader().loadClass("plus.bio.ser.proc." + name);
    r.register(c, s);
  }

  private URLClassLoader getClassLoader() throws MalformedURLException {
    List<URL> urls = new ArrayList<>();
    urls.add(new File("src/test/java").toURI().toURL());
    urls.add(outDir.toUri().toURL());
    return new URLClassLoader(urls.toArray(new URL[] {}));
  }

  private void writeClass(Compilation c, String name) throws IOException {
    name = name + ".class";
    JavaFileObject obj = c.generatedFile(StandardLocation.CLASS_OUTPUT, pkgPrefix + name).get();
    Path dest = outDir.resolve(pkgPrefix + name);
    FileOutputStream writer = new FileOutputStream(dest.toFile());
    ByteStreams.copy(obj.openInputStream(), writer);
  }

  private DataInputStream in() {
    ByteArrayInputStream in = new ByteArrayInputStream(outArray.toByteArray());
    return new DataInputStream(in);
  }

  @SuppressWarnings("serial")
  class FailedCompilation extends IOException {

    Compilation compilation;

    FailedCompilation(Compilation c) {
      compilation = c;
    }
  };
}
