package plus.bio.ser.proc;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;

import plus.bio.ser.proc.AutoSerializerProcessor;

public class ProcessorTest {

  static String pkgPrefix = "plus/bio/ser/";
  static String classpathPrefix = "src/test/java/" + pkgPrefix;

  private Path outDir;

  @Before
  public void setUp() throws IOException {
    outDir = Files.createTempDirectory("ser");
    outDir.resolve(pkgPrefix).toFile().mkdirs();
  }

  @After
  public void tearDown() throws IOException {
    Files.walkFileTree(outDir, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        Files.delete(file);
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        Files.delete(dir);
        return FileVisitResult.CONTINUE;
      }
    });
  }

  @Test
  public void testCompilation() {
    Compilation compilation =
      javac()
      .withProcessors(new AutoSerializerProcessor())
      .compile(JavaFileObjects.forResource("ThingToSerialize.java"));
    assertThat(compilation).succeeded();
  }
}
