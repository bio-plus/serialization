package plus.bio.ser.proc;

import javax.annotation.processing.Messager;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

/**
 * Used to encapuslate a failure during annotation processing.
 */
public class ProcessingException extends Exception {
  private static final long serialVersionUID = 1L;

  private final Element element;

  public ProcessingException(Element element, String msg, Object... args) {
    super(String.format(msg, args));
    this.element = element;
  }

  public Element getElement() {
    return element;
  }

  public void error(Messager msg) {
    msg.printMessage(Diagnostic.Kind.ERROR, toString(), getElement());
  }
}
