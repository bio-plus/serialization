package plus.bio.ser.proc;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

import com.squareup.javapoet.CodeBlock;

class Constructor {
  private final ExecutableElement constructor;
  private final List<AutoSerializerField> fields;
  private final Name name;

  public Constructor(TypeElement annotated) {
    this(annotated, null, new ArrayList<AutoSerializerField>());
  }

  public Constructor(TypeElement annotated, ExecutableElement c, List<AutoSerializerField> f) {
    constructor = c;
    fields = f;
    name = annotated.getQualifiedName();
  }

  public ExecutableElement getConstructor() {
    return constructor;
  }

  public String constructorName() {
    return name.toString();
  }

  public boolean isDefault() {
    return constructor == null;
  }

  public boolean hasParam(AutoSerializerField f) {
    return fields.contains(f);
  }

  public CodeBlock constructorCall(VariableElement var) {
    return CodeBlock.of("new $N()", constructorName());
  }
}
