package plus.bio.ser.proc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import com.google.auto.common.MoreElements;
import com.google.common.base.Joiner;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.SortedSetMultimap;
import com.google.common.collect.TreeMultimap;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeVariableName;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.anno.Deserializer;
import plus.bio.ser.anno.SerializedField;
import plus.bio.ser.anno.UseRegisteredSerializer;
import plus.bio.ser.api.GeneratedSerializer;
import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Serializer;
import plus.bio.ser.api.VersionedSerializer;

/**
 * Class representing a serializer, as specified by a Class annotated with {@link AutoSerializer}
 * and fields annotated with {@link SerializedField}.
 *
 * @author Tom Davis
 */
class AutoSerializerSpec {
  private static final String BOOL_SER_NAME = "booleanSerializer";
  private static final String CLASS_SER_NAME = "classSerializer";

  private final SortedSetMultimap<TypeElement, AutoSerializerField> fields =
      TreeMultimap.create(keyComparator, Ordering.<AutoSerializerField>natural());

  private final TypeElement annotated;
  private final AutoSerializer annotation;
  private final Types typeUtils;
  private final Elements elemUtils;
  private final ProcessingEnvironment env;
  private ExecutableElement sizeMethod;
  private ExecutableElement writeMethod;
  private ExecutableElement readMethod;
  private Constructor constructor;


  AutoSerializerSpec(TypeElement a, ProcessingEnvironment env) throws ProcessingException {
    annotated = a;
    annotation = annotated.getAnnotation(AutoSerializer.class);
    typeUtils = env.getTypeUtils();
    elemUtils = env.getElementUtils();
    this.env = env;

    findSerializerMethods();
    processFields();
    processConstructors();
    validate();
  }

  /**
   * Provides the name of the class to generate; defaults to the annotated class with the suffix
   * "Serializer".
   *
   * @return Name of serializer class
   */
  public String getSerializerName() {
    return annotation.name().equals("") ? annotated.getSimpleName() + "Serializer"
        : annotation.name();
  }

  /**
   * Provides the package for the class to generate; defaults to the same package as the annotated
   * class.
   *
   * @return Package of serializer class
   */
  public String getSerializerPackage() {
    return annotation.pkg().equals("")
        ? MoreElements.getPackage(annotated).getQualifiedName().toString() : annotation.pkg();
  }

  private TypeName getAnnotatedTypeName() {
    return ClassName.get(annotated);
  }

  public ClassName getClassName() {
    return ClassName.get(annotated);
  }

  List<FieldSpec> getSerializerFields() {
    final List<FieldSpec> ret = new ArrayList<>();
    for (Entry<String, Collection<AutoSerializerField>> entry : defineSerializers(true).fieldMap.asMap().entrySet()) {
      String name = entry.getKey();
      AutoSerializerField f = entry.getValue().iterator().next();

      TypeName type;
      if (f.useDefaultSerializer()) {
        type = ParameterizedTypeName.get(ClassName.get(Serializer.class), TypeName.get(f.fieldType()).box());
      } else {
        type = TypeName.get(f.getSpecifiedSerializer().asType());
      }
      ret.add(FieldSpec.builder(type, name, Modifier.PRIVATE).build());
    }
    ret.add(FieldSpec.builder(ParameterizedTypeName.get(ClassName.get(Serializer.class),
                                                        TypeName.get(Boolean.class)),
                                BOOL_SER_NAME, Modifier.PRIVATE).build());
    ret.add(FieldSpec.builder(ParameterizedTypeName.get(ClassName.get(Serializer.class),
                                                        TypeName.get(Class.class)),
                                CLASS_SER_NAME, Modifier.PRIVATE).build());
    return ret;
  }

  /**
   * Defines a block of variables <code>ser0, ser1,... serX</code> holding {@link Serializer}
   * instances.
   * <p>
   * This method de-duplicates serializers of the same type; for instance, there will only be one
   * {@link Long} serializer, etc.
   *
   * @param fromRegistry Whether the serializer should come from the Registry or instantiated
   *        directly
   * @return Object with the code block and a method for finding the right variable for a given
   *         field
   */
  private SerializerDefines defineSerializers(boolean fromRegistry) {
    final TypeElement noopSerializer =
        elemUtils.getTypeElement(UseRegisteredSerializer.class.getName());
    final TypeElement collectionSerializer =
      elemUtils.getTypeElement("plus.bio.ser.serializers.CollectionSerializer");
    final TypeElement mapSerializer = elemUtils.getTypeElement("plus.bio.ser.serializers.MapSerializer");
    final TypeElement missingExc =
        elemUtils.getTypeElement(MissingSerializerException.class.getName());
    final CodeBlock.Builder cb = CodeBlock.builder();
    final VariableElement registry = sizeMethod.getParameters().get(0);
    final Multimap<String, AutoSerializerField> map = ArrayListMultimap.create();
    final Map<TypeMirror, String> serToName = new HashMap<>();

    int counter = 0;
    for (Entry<TypeElement, AutoSerializerField> entry : fields.entries()) {
      AutoSerializerField field = entry.getValue();
      boolean usingExisting = false;

      for (Entry<TypeMirror, String> e : serToName.entrySet()) {
        if (typeUtils.isSameType(e.getKey(), field.fieldType())) {
          map.put(e.getValue(), field);
          usingExisting = true;
          break;
        }
      }

      if (usingExisting || (!fromRegistry && entry.getKey().equals(noopSerializer))) {
        continue;
      }

      String name = "ser" + counter++;

      if (field.isCollection()) {
        final List<TypeMirror> params = field.fieldParameters();
        cb.addStatement("$N = $T.create(new $T(), $T.class)",
            name, collectionSerializer, field.getCollectionProvider(), TypeName.get(params.get(0)));
      } else if (field.isMap()) {
        final List<TypeMirror> params = field.fieldParameters();
        cb.addStatement("$N = $T.create(new $T(), $T.class, $T.class)",
            name, mapSerializer, field.getMapProvider(), TypeName.get(params.get(0)),
            TypeName.get(params.get(1)));
      } else if (fromRegistry && entry.getKey().equals(noopSerializer)) {
        cb.addStatement("$N = $L.serializer($T.class)",
            name, registry.getSimpleName(), TypeName.get(field.fieldType()).box());
        cb.addStatement("if ($N == null) throw new $T($T.class)", name, missingExc,
            TypeName.get(field.fieldType()).box());
      } else {
        cb.addStatement("$N = new $T()", name, TypeName.get(entry.getKey().asType()));
      }
      map.put(name, field);
      serToName.put(field.fieldType(), name);
    }

    if (fromRegistry) {
      cb.addStatement("$N = $L.serializer($T.class)",
                      BOOL_SER_NAME,
                      registry.getSimpleName(),
                      TypeName.get(Boolean.class));
      cb.addStatement("if ($N == null) throw new $T($T.class)", BOOL_SER_NAME, missingExc,
                      TypeName.get(Boolean.class));
      cb.addStatement("$N = $L.serializer($T.class)",
                      CLASS_SER_NAME,
                      registry.getSimpleName(),
                      TypeName.get(Class.class));
      cb.addStatement("if ($N == null) throw new $T($T.class)", CLASS_SER_NAME, missingExc,
                      TypeName.get(Class.class));
    }

    return new SerializerDefines(map, cb.build());
  }

  /**
   * A manual implementation of {@link MethodSpec.overriding(ExecutableElement)} that doesn't
   * require a DeclaredType to resolve parameters, since we're generating said type as part of this.
   *
   * @param method Method to override
   * @return Builder for overridden method
   */
  private MethodSpec.Builder overrideSerializerMethod(ExecutableElement method) {
    boolean isTypeVar;
    String methodName = method.getSimpleName().toString();
    MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder(methodName);

    methodBuilder.addAnnotation(Override.class);

    Set<Modifier> modifiers = method.getModifiers();
    modifiers = new LinkedHashSet<>(modifiers);
    modifiers.remove(Modifier.ABSTRACT);
    methodBuilder.addModifiers(modifiers);

    List<ParameterSpec> parameters = new ArrayList<>();
    for (VariableElement parameter : method.getParameters()) {
      isTypeVar = parameter.asType().getKind() == TypeKind.TYPEVAR;
      parameters
        .add(
            isTypeVar
                ? ParameterSpec
                  .builder(getAnnotatedTypeName(), parameter.getSimpleName().toString()).build()
                : ParameterSpec.get(parameter));
    }

    for (TypeParameterElement typeParameterElement : method.getTypeParameters()) {
      TypeVariable var = (TypeVariable) typeParameterElement.asType();
      methodBuilder.addTypeVariable(TypeVariableName.get(var));
    }

    isTypeVar = method.getReturnType().getKind() == TypeKind.TYPEVAR;
    methodBuilder
      .returns(isTypeVar ? getAnnotatedTypeName() : TypeName.get(method.getReturnType()));
    methodBuilder.addParameters(parameters);
    methodBuilder.varargs(method.isVarArgs());

    for (TypeMirror thrownType : method.getThrownTypes()) {
      methodBuilder.addException(TypeName.get(thrownType));
    }

    return methodBuilder;
  }

  /**
   * @return Spec for {@link Serializer#size()}
   */
  private MethodSpec sizeMethod() {
    final String countVar = "count";
    final SerializerDefines ser = defineSerializers(true);
    final CodeBlock.Builder counts = CodeBlock.builder();
    final VariableElement registry = sizeMethod.getParameters().get(0);
    for (AutoSerializerField f : sorted(fields.values())) {
      CodeBlock sizeCall = CodeBlock.builder()
        .addStatement("$1N += $4L.size($2L, $3L)", countVar,
                      registry.getSimpleName().toString(),
                      f.getterCall(sizeMethod.getParameters().get(1)),
                      ser.labelForField(f))
        .build();
      if (f.isNullable()) {
        counts
          .addStatement("$1N += $4L.size($2L, $3L != null)",
                          countVar,
                          registry.getSimpleName().toString(),
                          f.getterCall(sizeMethod.getParameters().get(1)),
                          BOOL_SER_NAME)
          .add(CodeBlock.builder()
               .beginControlFlow("if ($L != null)", f.getterCall(sizeMethod.getParameters().get(1)))
               .add(sizeCall)
               .endControlFlow()
               .build());
      } else {
        counts.add(sizeCall);
      }

    }

    return overrideSerializerMethod(sizeMethod)
      .addStatement("defineSerializers($L)", registry.getSimpleName().toString())
      .addStatement("int $N = 0", countVar)
      .addCode(counts.build())
      .addStatement("return $N", countVar)
      .build();
  }

  /**
   * @return Spec for {@link Serializer#read()}
   */
  private MethodSpec readMethod() {
    final String objVar = "obj";
    final SerializerDefines ser = defineSerializers(true);
    final CodeBlock.Builder sets = CodeBlock.builder();
    final CodeBlock.Builder params = CodeBlock.builder();
    final List<String> paramNames = new ArrayList<>();
    final String registry = readMethod.getParameters().get(0).getSimpleName().toString();
    final String in = readMethod.getParameters().get(1).getSimpleName().toString();

    for (AutoSerializerField f : sorted(fields.values())) {
      CodeBlock param = CodeBlock.of("$L.read($N, $N)", ser.labelForField(f), registry, in);
      if (f.hasSetter()) {
        CodeBlock setterCall = f.setterCall(objVar, param);
        if (f.isNullable()) {
          setterCall = CodeBlock.builder()
            .beginControlFlow("if ($L.read($N, $N))", BOOL_SER_NAME, registry, in)
            .add(setterCall)
            .endControlFlow()
            .build();
        }
        sets.add(setterCall);
      } else {
        CodeBlock nullCheck = null;
        if (f.isNullable()) {
          nullCheck = CodeBlock.of("$L.read($N, $N)", BOOL_SER_NAME, registry, in);
        }
        params.add(f.localDefineCall(param, nullCheck));
        paramNames.add(f.fieldName());
      }
    }
    MethodSpec.Builder builder = overrideSerializerMethod(readMethod)
      .addStatement("defineSerializers($L)", registry)
      .addCode(params.build());

    builder.addStatement("final $1T $2N = new $1T($3L)", getAnnotatedTypeName(), objVar,
                         Joiner.on(",").join(paramNames));

      return builder
        .addCode(sets.build())
        .addStatement("return $N", objVar)
        .build();
  }

  /**
   * @return Spec for {@link Serializer#write()}
   */
  private MethodSpec writeMethod() {
    final SerializerDefines ser = defineSerializers(true);
    final CodeBlock.Builder writes = CodeBlock.builder();
    final VariableElement registry = writeMethod.getParameters().get(0);
    final VariableElement out = writeMethod.getParameters().get(1);
    final VariableElement obj = writeMethod.getParameters().get(2);

    for (AutoSerializerField f : sorted(fields.values())) {
      CodeBlock stmt = CodeBlock.builder()
        .addStatement("$L.write($N, $N, $L)", ser.labelForField(f), ParameterSpec.get(registry),
                      ParameterSpec.get(out), f.getterCall(obj))
        .build();
      if (f.isNullable()) {
        writes.addStatement("$L.write($N, $N, $L != null)", BOOL_SER_NAME, ParameterSpec.get(registry),
                            ParameterSpec.get(out), f.getterCall(obj));
        stmt = CodeBlock.builder()
          .beginControlFlow("if ($L != null)", f.getterCall(obj))
          .add(stmt)
          .endControlFlow()
          .build();
      }
      writes.add(stmt);
    }
    return overrideSerializerMethod(writeMethod)
      .addStatement("defineSerializers($L)", registry.getSimpleName().toString())
      .addCode(writes.build())
      .build();
  }

  /**
   * @return Spec for {@link GeneratedSerializer#registerWith()}
   */
  private MethodSpec registrarMethod() {
    final ExecutableElement method =
        getMethod(elemUtils.getTypeElement(GeneratedSerializer.class.getName()), "registerWith");
    SerializerDefines sers = defineSerializers(false);
    CodeBlock.Builder registrations = CodeBlock.builder();
    for (AutoSerializerField f : sers.fieldMap.values()) {
      TypeMirror unboxed;
      if (f.fieldType() instanceof PrimitiveType) {
        unboxed = typeUtils.boxedClass((PrimitiveType) f.fieldType()).asType();
      } else {
        unboxed = f.fieldType();
      }

      if (f.useDefaultSerializer()) {
        registrations.addStatement("$N.register($L.class, $L)",
          method.getParameters().get(0).getSimpleName(), unboxed, sers.labelForField(f));
      }
    }

    registrations.addStatement("$N.register($T.class, new $L())",
        method.getParameters().get(0).getSimpleName(), annotated, getSerializerName());

    CodeBlock impl = CodeBlock.builder().add(registrations.build()).build();

    return MethodSpec.overriding(method).addCode(impl).build();
  }

  private MethodSpec serDefineMethod() {
    final SerializerDefines sd = defineSerializers(true);
    final String firstName = sd.fieldMap.keySet().iterator().next();
    final TypeMirror registry = sizeMethod.getParameters().get(0).asType();
    return MethodSpec.methodBuilder("defineSerializers")
      .addParameter(TypeName.get(registry), "arg0")
      .addModifiers(Modifier.PRIVATE)
      .addException(TypeName.get(elemUtils.getTypeElement(MissingSerializerException.class.getName()).asType()))
      .beginControlFlow("if ($L == null)", firstName)
      .addCode(sd.block)
      .endControlFlow()
      .build();
  }

  /**
   * @return Spec for {@link VersionedSerializer#version()}
   */
  private MethodSpec versionMethod() {
    final ExecutableElement method =
        getMethod(elemUtils.getTypeElement(VersionedSerializer.class.getName()), "version");
    CodeBlock impl = CodeBlock.of("return $L;\n", annotation.version());
    return MethodSpec.overriding(method).addCode(impl).build();
  }

  List<MethodSpec> getMethods() {
    List<MethodSpec> res = new ArrayList<>();
    res.add(sizeMethod());
    res.add(readMethod());
    res.add(writeMethod());
    res.add(registrarMethod());
    res.add(versionMethod());
    res.add(serDefineMethod());
    return res;
  }

  // TODO (tdavis): Rewrite to use getMethod()
  private void findSerializerMethods() throws ProcessingException {
    TypeElement set = elemUtils.getTypeElement(Serializer.class.getName());
    final ImmutableSet<ExecutableElement> methods =
        MoreElements.getLocalAndInheritedMethods(set, typeUtils, elemUtils);
    for (ExecutableElement m : methods) {
      switch (m.getSimpleName().toString()) {
        case "size":
          sizeMethod = m;
          break;
        case "read":
          readMethod = m;
          break;
        case "write":
          writeMethod = m;
          break;
      }
    }
    for (ExecutableElement e : new ExecutableElement[] {sizeMethod, readMethod, writeMethod}) {
      if (e == null) {
        throw new ProcessingException(set,
            "`Serializer` interface missing expected method, one of: `size`, `read`, or `write`");
      }
    }

  }

  private void processConstructors() throws ProcessingException {
    // Default value; may be overridden later
    constructor = new Constructor(annotated);

    final List<Element> elements = new ArrayList<>(annotated.getEnclosedElements());
    elements.addAll(typeUtils.asElement(annotated.getSuperclass()).getEnclosedElements());
    final List<ExecutableElement> constructors = ElementFilter.constructorsIn(elements);

    for (ExecutableElement e : constructors) {
      if (!MoreElements.isAnnotationPresent(e, Deserializer.class)) {
        continue;
      }

      final List<? extends VariableElement> params = e.getParameters();
      final List<AutoSerializerField> autoParams = new ArrayList<>();
      for (VariableElement p : params) {
        TypeMirror t = p.asType();
        for (AutoSerializerField f : fields.values()) {
          if (typeUtils.isSameType(f.getField().asType(), t) && f.fieldName().equals(p.getSimpleName().toString())) {
            autoParams.add(f);
          }
        }
      }
      constructor = new Constructor(annotated, e, autoParams);
      break;
    }
  }

  /**
   * Find all fields with {@link SerializedField} annotation and their getters/setters
   *
   *
   * @throws ProcessingException If a getter or setter is missing
   */
  private void processFields() throws ProcessingException {
    ImmutableSet<ExecutableElement> methods =
        MoreElements.getLocalAndInheritedMethods(annotated, typeUtils, elemUtils);

    List<Element> elements = new ArrayList<>(annotated.getEnclosedElements());
    elements.addAll(typeUtils.asElement(annotated.getSuperclass()).getEnclosedElements());
    for (VariableElement e : ElementFilter.fieldsIn(elements)) {
      if (!MoreElements.isAnnotationPresent(e, SerializedField.class)) {
        continue;
      }

      final String fieldName = e.getSimpleName().toString();
      final String nameUpper = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
      final String getterName = "get" + nameUpper;
      final String setterName = "set" + nameUpper;

      ExecutableElement getter = null;
      ExecutableElement setter = null;
      for (ExecutableElement m : methods) {
        if (m.getSimpleName().toString().equals(getterName)) {
          getter = m;
        }
        if (m.getSimpleName().toString().equals(setterName)) {
          setter = m;
        }
      }

      final AutoSerializerField asf = new AutoSerializerField(env, e, getter, setter);
      fields.put(asf.getSpecifiedSerializer(), asf);
    }
  }

  private void validate() throws ProcessingException {

    for (AutoSerializerField f : fields.values()) {
      if (!f.hasGetter()) {
        throw new ProcessingException(f.getField(), "Field `%s` missing getter `%s`", f.fieldName(),
            f.getterName());
      } else if (!f.hasSetter() && (constructor.isDefault() || !constructor.hasParam(f))) {
        throw new ProcessingException(f.getField(),
            "Field `%s` missing setter `%s` and no custom constructor with the field was specified.",
            f.fieldName(), f.setterName());
      }
    }
  }

  /**
   * Get the method in {@code element} named {@code methodName}
   *
   * @param element an element representing an entity with a method name of {@code methodName}
   * @param methodName the method name
   * @return the method
   */
  private ExecutableElement getMethod(Element element, String methodName) {
    for (ExecutableElement executable : ElementFilter.methodsIn(element.getEnclosedElements())) {
      if (executable.getSimpleName().toString().equals(methodName)) {
        return executable;
      }
    }
    throw new IllegalArgumentException("no element named " + methodName + " + in element");
  }

  private List<AutoSerializerField> sorted(Collection<AutoSerializerField> c) {
    final List<AutoSerializerField> ret = new ArrayList<>();
    final List<AutoSerializerField> withSetter = new ArrayList<>();
    final List<AutoSerializerField> withoutSetter = new ArrayList<>();
    for (AutoSerializerField f : c) {
      if (f.hasSetter()) {
        withSetter.add(f);
      } else {
        withoutSetter.add(f);
      }
    }
    Collections.sort(withoutSetter);
    Collections.sort(withSetter);
    ret.addAll(withoutSetter);
    ret.addAll(withSetter);
    return ret;
  }

  static Comparator<TypeElement> keyComparator = new Comparator<TypeElement>() {

    @Override
    public int compare(TypeElement o1, TypeElement o2) {
      return o1.getSimpleName().toString().compareTo(o2.getSimpleName().toString());
    }
  };

  class SerializerDefines {
    final Multimap<String, AutoSerializerField> fieldMap;
    final CodeBlock block;

    public SerializerDefines(Multimap<String, AutoSerializerField> map, CodeBlock cb) {
      fieldMap = map;
      block = cb;
    }

    public String labelForField(AutoSerializerField f) {
      for (Entry<String, AutoSerializerField> entry : fieldMap.entries()) {
        if (f.equals(entry.getValue())) {
          return entry.getKey();
        }
      }
      return null;
    }

  }
}
