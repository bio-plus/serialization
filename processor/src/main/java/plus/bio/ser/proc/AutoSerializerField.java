package plus.bio.ser.proc;

import java.util.List;

import javax.annotation.Nullable;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import com.google.auto.common.MoreElements;
import com.google.auto.common.MoreTypes;
import com.google.common.base.Preconditions;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;

import plus.bio.ser.anno.SerializedField;
import plus.bio.ser.anno.UseRegisteredSerializer;

class AutoSerializerField implements Comparable<AutoSerializerField> {

  private final VariableElement field;
  private final ProcessingEnvironment env;

  private ExecutableElement getter;
  private ExecutableElement setter;

  public AutoSerializerField(ProcessingEnvironment e, VariableElement f, ExecutableElement g,
      ExecutableElement s) {
    env = e;
    field = f;
    getter = g;
    setter = s;
  }

  public VariableElement getField() {
    return field;
  }

  public String fieldName() {
    return field.getSimpleName().toString();
  }

  public TypeMirror fieldType() {
    return field.asType();
  }

  @SuppressWarnings("unchecked")
  public List<TypeMirror> fieldParameters() {
    return (List<TypeMirror>) MoreTypes.asDeclared(fieldType()).getTypeArguments();
  }

  public CodeBlock getterCall(VariableElement var) {
    final Types types = env.getTypeUtils();
    TypeMirror utilOptional;
    try {
       utilOptional = env.getElementUtils().getTypeElement("java.util.Optional").asType();
    } catch (NullPointerException e) {
       utilOptional = null;
    }
    TypeMirror guavaOptional;
    try {
      guavaOptional = env.getElementUtils().getTypeElement("com.google.common.base.Optional").asType();
    } catch (NullPointerException e) {
      guavaOptional = null;
    }
    final TypeMirror returnType = types.erasure(getter.getReturnType());
    String statement;

    if (utilOptional != null && types.isAssignable(returnType, utilOptional)) {
      statement = "$N.$N().orElse(null)";
    } else if (guavaOptional != null && types.isAssignable(returnType, guavaOptional)) {
      statement = "$N.$N().orNull()";
    } else {
      statement = "$N.$N()";
    }

    // NB: This *must* be a raw code block and not a statement as it is used inline.
    return CodeBlock.of(statement, ParameterSpec.get(var), getter.getSimpleName().toString());
  }

  public CodeBlock setterCall(String var, CodeBlock param) {
    final CodeBlock.Builder builder = CodeBlock.builder();
    return builder
      .addStatement("$N.$N($L)", var, setter.getSimpleName().toString(), param)
      .build();
  }

  public CodeBlock localDefineCall(CodeBlock param, CodeBlock nullCheck) {
    if (nullCheck == null) {
      return CodeBlock.builder()
        .addStatement("final $T $N = $L", fieldType(), fieldName(), param)
        .build();
    } else {
      return CodeBlock.builder()
        .addStatement("final $T $N = $L ? $L : null", fieldType(), fieldName(), nullCheck, param)
        .build();
    }
  }

  public boolean isNullable() {
    return field.getAnnotation(Nullable.class) != null;
  }

  public boolean hasSetter() {
    return setter != null;
  }

  public boolean hasGetter() {
    return getter != null;
  }

  public boolean isCollection() {
    final TypeMirror collection =
        env.getElementUtils().getTypeElement("java.util.Collection").asType();
    final Types types = env.getTypeUtils();
    return types.isAssignable(types.erasure(fieldType()), collection);
  }

  public boolean isMap() {
    final TypeMirror map = env.getElementUtils().getTypeElement("java.util.Map").asType();
    final Types types = env.getTypeUtils();
    return types.isAssignable(types.erasure(fieldType()), map);
  }

  public String getterName() {
    final String nameUpper = fieldName().substring(0, 1).toUpperCase() + fieldName().substring(1);
    return "get" + nameUpper;
  }

  public String setterName() {
    final String nameUpper = fieldName().substring(0, 1).toUpperCase() + fieldName().substring(1);
    return "set" + nameUpper;
  }

  public boolean useDefaultSerializer() {
    final Elements elems = env.getElementUtils();
    final TypeElement noopSerializer = elems.getTypeElement(UseRegisteredSerializer.class.getName());
    return getSpecifiedSerializer().equals(noopSerializer);
  }

  public TypeElement getSpecifiedSerializer() {
    final Types types = env.getTypeUtils();
      try {
        field.getAnnotation(SerializedField.class).serializer();
      } catch (MirroredTypeException mte) {
        return MoreElements.asType(types.asElement(mte.getTypeMirror()));
      }
      return null;
  }

  public ParameterizedTypeName getCollectionProvider() {
    Preconditions.checkArgument(isCollection(),
        "Tried to get CollectionProvider for non-Collection field");
    final SerializedField anno = field.getAnnotation(SerializedField.class);
    final Elements elems = env.getElementUtils();
    final Types types = env.getTypeUtils();
    final TypeElement noopProvider = elems.getTypeElement("plus.bio.ser.api.CollectionProvider");
    TypeElement cls = noopProvider;
    try {
      anno.collectionProvider();
    } catch (MirroredTypeException mte) {
      cls = MoreElements.asType(types.asElement(mte.getTypeMirror()));
    }

    if (cls.equals(noopProvider)) {
      cls = MoreElements.asType(elems.getTypeElement("plus.bio.ser.provider.ArrayListProvider"));
    }
    List<TypeMirror> params = fieldParameters();
    TypeName[] paramsArr = new TypeName[params.size()];
    for (int x = 0; x < params.size(); x++) {
      paramsArr[x] = TypeName.get(params.get(x));
    }
    return ParameterizedTypeName.get(ClassName.get(cls), paramsArr);
  }

  public ParameterizedTypeName getMapProvider() {
    Preconditions.checkArgument(isMap(), "Tried to get MapProvider for non-Map field");
    final SerializedField anno = field.getAnnotation(SerializedField.class);
    final Elements elems = env.getElementUtils();
    final Types types = env.getTypeUtils();
    final TypeElement noopProvider = elems.getTypeElement("plus.bio.ser.api.MapProvider");
    TypeElement cls = noopProvider;
    try {
      anno.mapProvider();
    } catch (MirroredTypeException mte) {
      cls = MoreElements.asType(types.asElement(mte.getTypeMirror()));
    }

    if (cls.equals(noopProvider)) {
      cls = MoreElements.asType(elems.getTypeElement("plus.bio.ser.provider.HashMapProvider"));
    }
    List<TypeMirror> params = fieldParameters();
    TypeName[] paramsArr = new TypeName[params.size()];
    for (int x = 0; x < params.size(); x++) {
      paramsArr[x] = TypeName.get(params.get(x));
    }
    return ParameterizedTypeName.get(ClassName.get(cls), paramsArr);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof AutoSerializerField)) {
      return false;
    }
    return ((AutoSerializerField) o).fieldName().equals(fieldName());
  }

  @Override
  public int compareTo(AutoSerializerField o) {
    return fieldName().compareTo(o.fieldName());
  }
}
