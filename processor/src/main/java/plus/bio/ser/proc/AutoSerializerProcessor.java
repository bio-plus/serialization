package plus.bio.ser.proc;

import static javax.lang.model.SourceVersion.RELEASE_7;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.processing.Messager;
import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.tools.Diagnostic;

import com.google.auto.common.BasicAnnotationProcessor;
import com.google.auto.common.MoreElements;
import com.google.auto.service.AutoService;
import com.google.common.collect.Lists;
import com.google.common.collect.SetMultimap;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import plus.bio.ser.anno.AutoSerializer;
import plus.bio.ser.api.GeneratedSerializer;
import plus.bio.ser.api.VersionedSerializer;

@SupportedSourceVersion(RELEASE_7)
@AutoService(Processor.class)
public class AutoSerializerProcessor extends BasicAnnotationProcessor {

  @Override
  protected Iterable<? extends ProcessingStep> initSteps() {
    return Lists.newArrayList(new Step0());
  }

  class Step0 implements ProcessingStep {

    @Override
    public Set<? extends Class<? extends Annotation>> annotations() {
      final Set<Class<? extends Annotation>> annos = new HashSet<>();
      annos.add(AutoSerializer.class);
      return annos;
    }

    @Override
    public Set<Element> process(SetMultimap<Class<? extends Annotation>, Element> elems) {
      final Set<Element> annotated = elems.get(AutoSerializer.class);
      final Messager msg = processingEnv.getMessager();
      for (Element e : annotated) {
        AutoSerializerSpec spec = null;
        try {
          spec = new AutoSerializerSpec(MoreElements.asType(e), processingEnv);
        } catch (ProcessingException exc) {
          exc.error(msg);
          return new HashSet<>();
        }

        TypeName serializerType = ClassName.get(spec.getSerializerPackage(), spec.getSerializerName());
        TypeName defaultRegistryType = ClassName.get("plus.bio.ser", "DefaultRegistry");
        FieldSpec field = FieldSpec
          .builder(serializerType, "instance", Modifier.PUBLIC, Modifier.STATIC)
          .initializer(CodeBlock.of("new $T()", serializerType))
          .build();
        TypeSpec ts = TypeSpec.classBuilder(spec.getSerializerName())
          .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
          .addSuperinterface(ParameterizedTypeName.get(ClassName.get(GeneratedSerializer.class),
              spec.getClassName()))
          .addSuperinterface(ClassName.get(VersionedSerializer.class))
          .addMethods(spec.getMethods())
          .addField(field)
          .addFields(spec.getSerializerFields())
          .addStaticBlock(CodeBlock.of("$L.registerWith($T.instance);", "instance", defaultRegistryType))
          .build();
        try {
          JavaFile.builder(spec.getSerializerPackage(), ts)
            .build()
            .writeTo(processingEnv.getFiler());
        } catch (IOException e1) {
          msg.printMessage(Diagnostic.Kind.ERROR, e1.toString());
        }
      }

      return new HashSet<>();
    }

  }
}
