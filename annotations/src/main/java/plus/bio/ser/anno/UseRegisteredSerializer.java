package plus.bio.ser.anno;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

/**
 * Internal "marker" class used by the processor; do not use!
 */
public final class UseRegisteredSerializer implements Serializer<Object> {

  public UseRegisteredSerializer() {
    throw new RuntimeException("Only used by annotation processor; should never instantiate!");
  }

  @Override
  public int size(Registry r, Object o) throws MissingSerializerException {
    return 0;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Object o) throws IOException {}

  @Override
  public Object read(Registry r, DataInputStream in) throws IOException {
    return null;
  }
}
