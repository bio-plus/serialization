package plus.bio.ser.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import plus.bio.ser.api.CollectionProvider;
import plus.bio.ser.api.MapProvider;
import plus.bio.ser.api.Serializer;

/**
 * Annotation used to add the annotated field to the enclosing class's
 * {@link plus.bio.ser.api.Serializer}.
 * <p>
 * This annotation requires {@link AutoSerializer} to be present on the enclosing class.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.CLASS)
public @interface SerializedField {

  /**
   * @return A custom serializer for the field; default to whatever is found in the
   *         {@link plus.bio.ser.api.Registry} at serialization-time.
   */
  Class<? extends Serializer<?>> serializer() default UseRegisteredSerializer.class;

  /**
   * @return A class implementing {@link CollectionProvider} for the given field type. Must have a
   *         no-argument constructor. Default is {@link plus.bio.ser.provider.ArrayListProvider}.
   */
  @SuppressWarnings("rawtypes")
  Class<? extends CollectionProvider> collectionProvider() default CollectionProvider.class;

  /**
   * @return A class implementing {@link MapProvider} for the given field type. Must have a
   *         no-argument constructor. Default is {@link plus.bio.ser.provider.HashMapProvider}.
   */
  @SuppressWarnings("rawtypes")
  Class<? extends MapProvider> mapProvider() default MapProvider.class;

}
