package plus.bio.ser.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to tell {@link plus.bio.ser.proc.AutoSerializerProcessor} to generate a
 * {@link plus.bio.ser.api.Serializer} for the annotated class.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface AutoSerializer {

  /**
   * @return Class name of the generated Serializer; defaults to the annotated class name with
   *         "Serializer" suffix
   */
  String name() default "";

  /**
   * @return Destination package of the generated Serializer; defaults to same package as the
   *         annotated Class
   */
  String pkg() default "";

  /**
   * @return Current serialization implementation version; increment this when the annotated class
   *         adds, removes, or changes fields.
   */
  int version() default 1;
}
