/**
 * Annotations used by {@link plus.bio.ser.proc.AutoSerializerProcessor}.
 */
package plus.bio.ser.anno;
