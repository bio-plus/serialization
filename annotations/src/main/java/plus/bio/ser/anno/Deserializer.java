package plus.bio.ser.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to add the annotated field to the enclosing class's
 * {@link plus.bio.ser.api.Serializer}.
 * <p>
 * This annotation requires {@link AutoSerializer} to be present on the enclosing class.
 */
@Target(ElementType.CONSTRUCTOR)
@Retention(RetentionPolicy.CLASS)
public @interface Deserializer {}
