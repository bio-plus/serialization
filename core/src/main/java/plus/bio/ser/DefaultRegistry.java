package plus.bio.ser;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Ordering;
import com.google.common.collect.SortedSetMultimap;
import com.google.common.collect.TreeMultimap;

import plus.bio.ser.api.MigrationStrategy;
import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.NoMigrationPathException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;
import plus.bio.ser.api.VersionedSerializer;
import plus.bio.ser.serializers.BooleanSerializer;
import plus.bio.ser.serializers.ByteSerializer;
import plus.bio.ser.serializers.CharSerializer;
import plus.bio.ser.serializers.ClassSerializer;
import plus.bio.ser.serializers.DateSerializer;
import plus.bio.ser.serializers.DoubleSerializer;
import plus.bio.ser.serializers.FloatSerializer;
import plus.bio.ser.serializers.IntegerSerializer;
import plus.bio.ser.serializers.InterfaceSerializer;
import plus.bio.ser.serializers.LongSerializer;
import plus.bio.ser.serializers.ShortSerializer;
import plus.bio.ser.serializers.StackTraceElementSerializer;
import plus.bio.ser.serializers.StringSerializer;
import plus.bio.ser.serializers.ThrowableSerializer;
import plus.bio.ser.serializers.UUIDSerializer;

/**
 * Standard implementation of a serializer/migration registry.
 * <p>
 * It's recommended that you use the static {@link instance} of this class as it already has
 * serializers generated via {@link plus.bio.ser.anno.AutoSerializer} registered. If you instead
 * instantiate a new instance, you'll need to call {@link plus.bio.ser.api.GeneratedSerializer#registerWith}
 * manually on each generated serializer you plan to use.
 * <p>
 * This implementation provides some extra niceties. Firstly, it automatically registers serializers
 * for primitive types and {@link String}. Secondly, it will provide versioning-aware serializers
 * via the {@link VersioningWrapper}, when a serializer implements {@link VersionedSerializer}.
 * <p>
 * Mapping of types (Class) to unique ID is done by taking the {@code hashCode()} of
 * {@code Class.getName()}.
 *
 * @author Tom Davis
 */
public class DefaultRegistry implements Registry {

  public static final Map<Class<?>, Serializer<?>> defaultRegistrations = ImmutableMap
    .<Class<?>, Serializer<?>>builder()
    .put(Long.class, new LongSerializer())
    .put(Boolean.class, new BooleanSerializer())
    .put(Character.class, new CharSerializer())
    .put(Byte.class, new ByteSerializer())
    .put(Short.class, new ShortSerializer())
    .put(Integer.class, new IntegerSerializer())
    .put(Float.class, new FloatSerializer())
    .put(Double.class, new DoubleSerializer())
    .put(String.class, new StringSerializer())
    .put(Date.class, new DateSerializer())
    .put(UUID.class, new UUIDSerializer())
    .put(StackTraceElement.class, new StackTraceElementSerializer())
    .put(Throwable.class, new ThrowableSerializer())
    .put(Class.class, new ClassSerializer())
    .build();

  private final Map<Class<?>, Serializer<?>> registry = new HashMap<>();
  private final SortedSetMultimap<Class<?>, MigrationStrategy> migrations =
      TreeMultimap.create(clsCompare, Ordering.<MigrationStrategy>natural());

  public DefaultRegistry() {
    registry.putAll(defaultRegistrations);
  };

  @Override
  public <T> void register(Class<T> c, Serializer<T> s) {
    registry.put(c, s);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> Serializer<T> serializer(Class<T> c) throws MissingSerializerException {

    if (c.isInterface()) {
      return new InterfaceSerializer<T>();
    }

    final Object s = registry.get(c);
    if (s == null) {
      throw new MissingSerializerException(c);
    }
    if (s instanceof VersionedSerializer) {
      return new VersioningWrapper<T>((Serializer<T>) s, c);
    }
    return (Serializer<T>) s;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> Serializer<T> serializer(int id) throws MissingSerializerException {
    for (Entry<Class<?>, Serializer<?>> e : registry.entrySet()) {
      if (e.getKey().getName().hashCode() == id) {
        return (Serializer<T>) e.getValue();
      }
    }
    throw new MissingSerializerException(id);
  }

  @Override
  public <T> int id(Class<T> c) {
    return c.getName().hashCode();
  }

  @Override
  public <T> void addMigration(Class<T> c, MigrationStrategy strategy) {
    migrations.put(c, strategy);
  }

  @Override
  public <T> SortedSet<MigrationStrategy> migrations(Class<T> c, int fromVersion, int toVersion)
      throws NoMigrationPathException {
    final Iterator<MigrationStrategy> avail = migrations.get(c).iterator();
    final SortedSet<MigrationStrategy> result = new TreeSet<>();

    int currentVersion = fromVersion;
    while (avail.hasNext()) {
      MigrationStrategy strat = avail.next();
      if (strat.fromVersion() == currentVersion) {
        result.add(strat);
        currentVersion = strat.toVersion();
      }
      if (currentVersion == toVersion) {
        return result;
      }
    }
    throw new NoMigrationPathException();
  }

  private static Comparator<Class<?>> clsCompare = new Comparator<Class<?>>() {

    @Override
    public int compare(Class<?> o1, Class<?> o2) {
      return o1.getName().compareTo(o2.getName());
    }
  };

  /**
   * Static instance of the Registry that automatically registers serializers generated via {@link
   * plus.bio.ser.anno.AutoSerializer}
   */
  public static final Registry instance = new DefaultRegistry();

}
