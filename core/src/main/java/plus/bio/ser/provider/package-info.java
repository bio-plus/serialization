/**
 * Library of built-in providers implementing {@link plus.bio.ser.api.CollectionProvider} and
 * {@link plus.bio.ser.api.MapProvider}.
 */
package plus.bio.ser.provider;
