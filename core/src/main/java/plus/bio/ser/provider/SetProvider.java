package plus.bio.ser.provider;

import java.util.HashSet;

import plus.bio.ser.api.CollectionProvider;

/**
 * A convience {@link CollectionProvider} that provides an {@link HashSet}
 */
public class SetProvider<T> implements CollectionProvider<T, HashSet<T>> {

  public HashSet<T> apply(int size) {
    return new HashSet<T>();
  };
}
