package plus.bio.ser.provider;

import java.util.HashMap;
import java.util.Map;

import plus.bio.ser.api.MapProvider;

/**
 * A convience {@link MapProvider} that provides an {@link HashMap}
 *
 * @author Tom Davis
 */
public class HashMapProvider<K, V> implements MapProvider<K, V> {

  public Map<K, V> apply(int size) {
    return new HashMap<K, V>(size);
  };
}
