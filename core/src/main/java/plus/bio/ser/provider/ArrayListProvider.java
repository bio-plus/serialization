package plus.bio.ser.provider;

import java.util.ArrayList;
import java.util.List;

import plus.bio.ser.api.CollectionProvider;

/**
 * A convience {@link CollectionProvider} that provides an {@link ArrayList}
 *
 * @author Tom Davis
 */
public class ArrayListProvider<T> implements CollectionProvider<T, List<T>> {

  public List<T> apply(int size) {
    return new ArrayList<>(size);
  };
}
