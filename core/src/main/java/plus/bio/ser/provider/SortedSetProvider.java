package plus.bio.ser.provider;

import java.util.SortedSet;
import java.util.TreeSet;

import plus.bio.ser.api.CollectionProvider;

/**
 * A convience {@link CollectionProvider} that provides an {@link SortedSet}
 *
 * @author Tom Davis
 */
public class SortedSetProvider<T> implements CollectionProvider<T, SortedSet<T>> {

  public SortedSet<T> apply(int size) {
    return new TreeSet<T>();
  };
}
