package plus.bio.ser;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.SortedSet;

import plus.bio.ser.api.MigrationStrategy;
import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;
import plus.bio.ser.api.VersionedSerializer;

/**
 * Wraps a normal {@link Serializer} implementing {@link VersionedSerializer} to read/write version
 * info and apply migrations.
 * <p>
 * The {@link plus.bio.ser.DefaultRegistry} will automatically wrap serializers provided by
 * {@link Registry#serializer(Class)} with this class.
 * <p>
 * Serializers failing to implement {@link VersionedSerializer} may cause {@link AssertionError}
 * to be thrown during initialization, provided the code was compiled with assertions.
 *
 * @author Tom Davis
 */
public class VersioningWrapper<T> implements Serializer<T>, VersionedSerializer {

  final Serializer<T> delegate;
  final VersionedSerializer vers;
  final Class<T> klass;

  public VersioningWrapper(Serializer<T> ser, Class<T> tClass) {
    assert ser instanceof VersionedSerializer;

    delegate = ser;
    klass = tClass;
    vers = (VersionedSerializer) ser; // Just to avoid casting all the time
  }

  @Override
  public int size(Registry r, T o) throws MissingSerializerException {
    return delegate.size(r, o) + 4;
  }

  @Override
  public void write(Registry r, DataOutputStream out, T o) throws IOException {
    out.writeInt(version());
    delegate.write(r, out, o);
  }

  @Override
  public T read(Registry r, DataInputStream in) throws IOException {
    final int version = in.readInt();
    if (version < vers.version()) {
      SortedSet<MigrationStrategy> strat = r.migrations(klass, version, version());
      in = Migrations.applyAll(strat, r, in);
    }
    return delegate.read(r, in);
  }

  @Override
  public int version() {
    return vers.version();
  }
}
