package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.SerializationException;
import plus.bio.ser.api.Serializer;

public class EnumSerializer<T extends Enum<T>> implements Serializer<T> {

  private final Serializer<String> nameDelegate = new StringSerializer();
  private final Serializer<Class<?>> classDelegate = new ClassSerializer();

  @SuppressWarnings("unchecked")
  @Override
  public int size(Registry r, T o) throws MissingSerializerException {
    return nameDelegate.size(r, o.name()) + classDelegate.size(r, (Class<T>) o.getClass());
  }

  @SuppressWarnings("unchecked")
  @Override
  public void write(Registry r, DataOutputStream out, T o)
      throws IOException, SerializationException {
    classDelegate.write(r, out, (Class<T>) o.getClass());
    nameDelegate.write(r, out, o.name());
  }

  @SuppressWarnings("unchecked")
  @Override
  public T read(Registry r, DataInputStream in) throws IOException {
    try {
      return Enum.valueOf((Class<T>)classDelegate.read(r, in), nameDelegate.read(r, in));
    } catch (IllegalArgumentException e) {
      throw new IOException(e);
    }
  }
}
