package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class StringSerializer implements Serializer<String> {

  @Override
  public int size(Registry r, String o) throws MissingSerializerException {
    // See: https://tools.ietf.org/html/rfc3629#section-3
    int count = 0;
    for (int i = 0, len = o.length(); i < len; i++) {
      char ch = o.charAt(i);
      if (ch <= 0x7F) {
        count++;
      } else if (ch <= 0x7FF) {
        count += 2;
      } else if (Character.isHighSurrogate(ch)) {
        count += 4;
        ++i;
      } else {
        count += 3;
      }
    }
    return count + 2;
  }

  @Override
  public void write(Registry r, DataOutputStream out, String o) throws IOException {
    out.writeUTF(o);
  }

  @Override
  public String read(Registry r, DataInputStream in) throws IOException {
    return in.readUTF();
  }

}
