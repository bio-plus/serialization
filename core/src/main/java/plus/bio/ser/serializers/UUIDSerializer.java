package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class UUIDSerializer implements Serializer<UUID> {

  private static final Serializer<String> delegate = new StringSerializer();

  @Override
  public int size(Registry r, UUID o) throws MissingSerializerException {
    return delegate.size(r, o.toString());
  }

  @Override
  public void write(Registry r, DataOutputStream out, UUID o) throws IOException {
    delegate.write(r, out, o.toString());
  }

  @Override
  public UUID read(Registry r, DataInputStream in) throws IOException {
    return UUID.fromString(delegate.read(r, in));
  }

}
