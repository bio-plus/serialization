package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class ByteSerializer implements Serializer<Byte> {

  @Override
  public int size(Registry r, Byte o) throws MissingSerializerException {
    return 1;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Byte o) throws IOException {
    out.writeByte(o);
  }

  @Override
  public Byte read(Registry r, DataInputStream in) throws IOException {
    return in.readByte();
  }

}
