package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.SerializationException;
import plus.bio.ser.api.Serializer;

public class ArraySerializer<T> implements Serializer<T[]> {

  private final Serializer<Class<?>> sCls = new ClassSerializer();

  @SuppressWarnings("unchecked")
  @Override
  public int size(Registry r, T[] o) throws MissingSerializerException {
    final Serializer<Integer> sInt = r.serializer(Integer.class);
    if (o.length == 0) {
      return sInt.size(r, 0);
    }
    final Class<T> cls = (Class<T>) o[0].getClass();
    final Serializer<T> sT = r.serializer(cls);
    int size = sCls.size(r, cls) + sInt.size(r, o.length);
    for (T t : o) {
      size += sT.size(r, t);
    }
    return size;
  }

  @SuppressWarnings("unchecked")
  @Override
  public void write(Registry r, DataOutputStream out, T[] o) throws IOException, SerializationException {
    final Serializer<Integer> sInt = r.serializer(Integer.class);

    sInt.write(r, out, o.length);
    if (o.length == 0) {
      return;
    }
    final Class<T> cls = (Class<T>) o[0].getClass();
    final Serializer<T> sT = r.serializer(cls);
    sCls.write(r, out, cls);
    for (T t : o) {
      sT.write(r, out, t);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public T[] read(Registry r, DataInputStream in) throws IOException {
    final Serializer<Integer> sInt = r.serializer(Integer.class);

    final int length = sInt.read(r, in);
    if (length == 0) {
      return null;
    }

    final Class<T> cls = (Class<T>) sCls.read(r, in);
    final Serializer<T> sT = r.serializer(cls);
    final T[] arr = (T[]) Array.newInstance(cls, length);
    for (int x = 0; x < length; x++) {
      arr[x] = sT.read(r, in);
    }
    return arr;
  }

}
