package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class FloatSerializer implements Serializer<Float> {

  @Override
  public int size(Registry r, Float o) throws MissingSerializerException {
    return 4;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Float o) throws IOException {
    out.writeFloat(o);
  }

  @Override
  public Float read(Registry r, DataInputStream in) throws IOException {
    return in.readFloat();
  }

}
