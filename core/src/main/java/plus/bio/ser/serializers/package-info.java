/**
 * Library of built-in serializers covering primitive types and {@link String}.
 */
package plus.bio.ser.serializers;
