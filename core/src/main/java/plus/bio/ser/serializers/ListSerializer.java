package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class ListSerializer {
  public static <T> Serializer<List<T>> create(final Class<T> klass) {
    return new Serializer<List<T>>() {

      @Override
      public int size(Registry r, List<T> o) throws MissingSerializerException {
        final int entries = o.size();

        if (entries == 0) {
          return 0;
        }

        final Serializer<T> serializer = r.serializer(klass);
        // size + type-id
        int count = 8;
        for (T e : o) {
          count += serializer.size(r, e);
        }
        return count;
      }

      @Override
      public void write(Registry r, DataOutputStream out, List<T> o) throws IOException {
        Serializer<T> serializer = null;

        out.writeInt(o.size());

        for(final T e : o) {
          if (serializer == null) {
            serializer = r.serializer(klass);
            out.writeInt(r.id(klass));
          }
          serializer.write(r, out, e);
        }
      }

      @Override
      public List<T> read(Registry r, DataInputStream in) throws IOException {
        final int size = in.readInt();
        final List<T> res = new ArrayList<>(size);
        final Serializer<T> serializer = r.serializer(in.readInt());

        for (int x=0; x<size; x++) {
          res.add(serializer.read(r, in));
        }
        return res;
      }
    };
  }
}
