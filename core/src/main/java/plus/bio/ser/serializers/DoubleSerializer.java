package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class DoubleSerializer implements Serializer<Double> {

  @Override
  public int size(Registry r, Double o) throws MissingSerializerException {
    return 8;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Double o) throws IOException {
    out.writeDouble(o);
  }

  @Override
  public Double read(Registry r, DataInputStream in) throws IOException {
    return in.readDouble();
  }

}
