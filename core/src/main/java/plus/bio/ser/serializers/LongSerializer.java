package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class LongSerializer implements Serializer<Long> {

  @Override
  public int size(Registry r, Long o) throws MissingSerializerException {
    return 8;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Long o) throws IOException {
    out.writeLong(o);
  }

  @Override
  public Long read(Registry r, DataInputStream in) throws IOException {
    return in.readLong();
  }

}
