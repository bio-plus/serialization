package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.google.common.base.Optional;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.SerializationException;
import plus.bio.ser.api.Serializer;

public class GuavaOptionalSerializer<T> implements Serializer<Optional<T>> {

  private final Class<T> c;
  private final Serializer<Boolean> b = new BooleanSerializer();

  public GuavaOptionalSerializer(Class<T> klass) {
    c = klass;
  }

  @Override
  public int size(Registry r, Optional<T> o) throws MissingSerializerException {
    int size = 0;
    size += b.size(r, o.isPresent());
    if (o.isPresent()) {
      Serializer<T> s = r.serializer(c);
      size += s.size(r, o.get());
    }
    return size;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Optional<T> o)
      throws IOException, SerializationException {
    b.write(r, out, o.isPresent());
    if (o.isPresent()) {
      Serializer<T> s = r.serializer(c);
      s.write(r, out, o.get());
    }
  }

  @Override
  public Optional<T> read(Registry r, DataInputStream in) throws IOException {
    boolean present = b.read(r, in);
    if (present) {
      Serializer<T> s = r.serializer(c);
      return Optional.of(s.read(r, in));
    }
    return Optional.absent();
  }

}
