package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class IntegerSerializer implements Serializer<Integer> {

  @Override
  public int size(Registry r, Integer o) throws MissingSerializerException {
    return 4;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Integer o) throws IOException {
    out.writeInt(o);
  }

  @Override
  public Integer read(Registry r, DataInputStream in) throws IOException {
    return in.readInt();
  }

}
