package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Date;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class DateSerializer implements Serializer<Date> {

  private static final Serializer<Long> delegate = new LongSerializer();

  @Override
  public int size(Registry r, Date o) throws MissingSerializerException {
    return delegate.size(r, o.getTime());
  }

  @Override
  public void write(Registry r, DataOutputStream out, Date o) throws IOException {
    delegate.write(r, out, o.getTime());
  }

  @Override
  public Date read(Registry r, DataInputStream in) throws IOException {
    return new Date(delegate.read(r, in));
  }

}
