package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.NullableWrapper;
import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.SerializationException;
import plus.bio.ser.api.Serializer;

public class StackTraceElementSerializer implements Serializer<StackTraceElement> {

  private Serializer<String> sStr;
  private Serializer<Integer> sInt;

  @Override
  public int size(Registry r, StackTraceElement o) throws MissingSerializerException {
    defineSerializers(r);
    return sStr.size(r, o.getClassName()) + sStr.size(r, o.getMethodName()) + sStr.size(r, o.getFileName()) + sInt.size(r, o.getLineNumber());
  }

  @Override
  public void write(Registry r, DataOutputStream out, StackTraceElement o)
      throws IOException, SerializationException {
    defineSerializers(r);
    sStr.write(r, out, o.getClassName());
    sStr.write(r, out, o.getMethodName());
    sStr.write(r, out, o.getFileName());
    sInt.write(r, out, o.getLineNumber());
  }

  @Override
  public StackTraceElement read(Registry r, DataInputStream in) throws IOException {
    defineSerializers(r);
    final String className = sStr.read(r, in);
    final String methodName = sStr.read(r, in);
    final String fileName = sStr.read(r, in);
    final int line = sInt.read(r, in);
    return new StackTraceElement(className, methodName, fileName, line);
  }

  private void defineSerializers(Registry r) throws MissingSerializerException {
    if (sStr == null) {
      sStr = new NullableWrapper<String>(String.class);
      sInt = r.serializer(Integer.class);
    }
  }
}
