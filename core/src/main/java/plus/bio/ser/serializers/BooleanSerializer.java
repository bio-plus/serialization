package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class BooleanSerializer implements Serializer<Boolean> {

  @Override
  public int size(Registry r, Boolean o) throws MissingSerializerException {
    return 1;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Boolean o) throws IOException {
    out.writeBoolean(o);
  }

  @Override
  public Boolean read(Registry r, DataInputStream in) throws IOException {
    return in.readBoolean();
  }

}
