package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import plus.bio.ser.NullableWrapper;
import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.SerializationException;
import plus.bio.ser.api.Serializer;

public class ThrowableSerializer implements Serializer<Throwable> {

  private Serializer<String> sStr;
  private Serializer<Throwable> sThrow;
  @SuppressWarnings("rawtypes")
  private Serializer<Class> sCls;
  private ArraySerializer<StackTraceElement> sStack;

  @Override
  public int size(Registry r, Throwable o) throws MissingSerializerException {
    defineSerializers(r);
    int size = 0;
    size += sCls.size(r, o.getClass());
    size += sStr.size(r, o.getMessage());
    size += sStack.size(r, o.getStackTrace());
    size += sThrow.size(r, o.getCause());
    return size;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Throwable o) throws IOException, SerializationException {
    defineSerializers(r);
    sCls.write(r, out, o.getClass());
    sStr.write(r, out, o.getMessage());
    sStack.write(r, out, o.getStackTrace());
    sThrow.write(r, out, o.getCause());
  }

  @Override
  public Throwable read(Registry r, DataInputStream in) throws IOException {
    defineSerializers(r);
    @SuppressWarnings("unchecked")
	final Class<? extends Throwable> cls = (Class<? extends Throwable>) sCls.read(r, in);
      final String message = sStr.read(r, in);
    final StackTraceElement[] stack = sStack.read(r, in);
    final Throwable cause = sThrow.read(r, in);

    try {
      final Throwable throwable = cls.getConstructor(String.class).newInstance(message);
      if (stack != null) {
        throwable.setStackTrace(stack);
      } else {
        throwable.setStackTrace(new StackTraceElement[]{});
      }
      if (cause != null) {
        throwable.initCause(cause);
      }
      return throwable;
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
        | NoSuchMethodException | SecurityException e) {
      throw new IOException(e);
    }
  }

  private void defineSerializers(Registry r) throws MissingSerializerException {
    if (sStr == null) {
      sStr = new NullableWrapper<String>(String.class);
      sThrow = new NullableWrapper<Throwable>(Throwable.class);
      sStack = new ArraySerializer<StackTraceElement>();
      sCls = r.serializer(Class.class);
    }
  }


}
