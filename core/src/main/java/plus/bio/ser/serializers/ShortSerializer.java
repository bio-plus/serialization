package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class ShortSerializer implements Serializer<Short> {

  @Override
  public int size(Registry r, Short o) throws MissingSerializerException {
    return 2;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Short o) throws IOException {
    out.writeShort(o);
  }

  @Override
  public Short read(Registry r, DataInputStream in) throws IOException {
    return in.readShort();
  }

}
