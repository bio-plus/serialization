package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import plus.bio.ser.api.MapProvider;
import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class MapSerializer {
  public static <K, V> Serializer<Map<K, V>> create(final MapProvider<K, V> provider, final Serializer<K> k, final Serializer<V> v) {
    return new Serializer<Map<K, V>>() {

      @Override
      public int size(Registry r, Map<K, V> o) throws MissingSerializerException {
        final int entries = o.entrySet().size();

        if (entries == 0) {
          return 0;
        }

        int count = 4; // size
        for (Entry<K, V> e : o.entrySet()) {
          count += k.size(r, e.getKey());
          count += v.size(r, e.getValue());
        }
        return count;
      }

      @Override
      public void write(Registry r, DataOutputStream out, Map<K, V> o) throws IOException {
        out.writeInt(o.entrySet().size());

        for (final Entry<K, V> e : o.entrySet()) {
          k.write(r, out, e.getKey());
          v.write(r, out, e.getValue());
        }
      }

      @Override
      public Map<K, V> read(Registry r, DataInputStream in) throws IOException {
        final int size = in.readInt();
        final Map<K, V> res = provider.apply(size);

        for (int x = 0; x < size; x++) {
          res.put(k.read(r, in), v.read(r, in));
        }
        return res;
      }
    };
  }

  public static <K, V> Serializer<Map<K, V>> create(
      final MapProvider<K, V> provider, final Class<K> k, final Class<V> v) {
    return new Serializer<Map<K, V>>() {

      @Override
      public int size(Registry r, Map<K, V> o) throws MissingSerializerException {
        final int entries = o.entrySet().size();

        if (entries == 0) {
          return 0;
        }

        Serializer<K> keySerializer = r.serializer(k);
        Serializer<V> valSerializer = r.serializer(v);

        int count = 12; // size + key-id + val-id
        for (Entry<K, V> e : o.entrySet()) {
          count += keySerializer.size(r, e.getKey());
          count += valSerializer.size(r, e.getValue());
        }
        return count;
      }

      @Override
      public void write(Registry r, DataOutputStream out, Map<K, V> o) throws IOException {
        Serializer<K> keySerializer = null;
        Serializer<V> valSerializer = null;

        out.writeInt(o.entrySet().size());

        for (final Entry<K, V> e : o.entrySet()) {
          if (keySerializer == null) {
            keySerializer = r.serializer(k);
            valSerializer = r.serializer(v);

            out.writeInt(r.id(k));
            out.writeInt(r.id(v));
          }
          keySerializer.write(r, out, e.getKey());
          valSerializer.write(r, out, e.getValue());
        }
      }

      @Override
      public Map<K, V> read(Registry r, DataInputStream in) throws IOException {
        final int size = in.readInt();
        final Map<K, V> res = provider.apply(size);
        final Serializer<K> keySerializer = r.serializer(in.readInt());
        final Serializer<V> valSerializer = r.serializer(in.readInt());

        for (int x = 0; x < size; x++) {
          res.put(keySerializer.read(r, in), valSerializer.read(r, in));
        }
        return res;
      }
    };
  }
}
