package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.SerializationException;
import plus.bio.ser.api.Serializer;

public class ClassSerializer implements Serializer<Class<?>> {

  private static Serializer<String> delegate = new StringSerializer();

  @Override
  public int size(Registry r, Class<?> o) throws MissingSerializerException {
    return delegate.size(r, o.getName());
  }

  @Override
  public void write(Registry r, DataOutputStream out, Class<?> o)
      throws IOException, SerializationException {
    delegate.write(r, out, o.getName());
  }

  @Override
  public Class<?> read(Registry r, DataInputStream in) throws IOException {
    try {
      return Class.forName(delegate.read(r, in));
    } catch (ClassNotFoundException e) {
      throw new IOException(e);
    }
  }

}
