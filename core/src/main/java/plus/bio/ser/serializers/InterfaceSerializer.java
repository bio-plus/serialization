package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.SerializationException;
import plus.bio.ser.api.Serializer;

public class InterfaceSerializer<T> implements Serializer<T> {

  private final ClassSerializer sCls = new ClassSerializer();

  @SuppressWarnings("unchecked")
  @Override
  public int size(Registry r, T o) throws MissingSerializerException {
    Class<T> oCls = (Class<T>)o.getClass();
    return sCls.size(r, o.getClass()) + r.serializer(oCls).size(r, o);
  }

  @SuppressWarnings("unchecked")
  @Override
  public void write(Registry r, DataOutputStream out, T o)
      throws IOException, SerializationException {
    Class<T> oCls = (Class<T>)o.getClass();
    sCls.write(r, out, oCls);
    r.serializer(oCls).write(r, out, o);
  }

  @SuppressWarnings("unchecked")
  @Override
  public T read(Registry r, DataInputStream in) throws IOException {
    Class<T> oCls = (Class<T>) sCls.read(r, in);
    return r.serializer(oCls).read(r, in);
  }
}
