package plus.bio.ser.serializers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;

public class CharSerializer implements Serializer<Character> {

  @Override
  public int size(Registry r, Character o) throws MissingSerializerException {
    return 2;
  }

  @Override
  public void write(Registry r, DataOutputStream out, Character o) throws IOException {
    out.writeChar(o);
  }

  @Override
  public Character read(Registry r, DataInputStream in) throws IOException {
    return in.readChar();
  }

}
