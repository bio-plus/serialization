package plus.bio.ser;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.SerializationException;
import plus.bio.ser.api.Serializer;

public class NullableWrapper<T> implements Serializer<T> {

  private final Class<T> delegate;

  public NullableWrapper(Class<T> delegate) {
    this.delegate = delegate;
  }

  @Override
  public int size(Registry r, T o) throws MissingSerializerException {
    final Serializer<Boolean> sBool = r.serializer(Boolean.class);
    int size = sBool.size(r, o != null);
    if (o != null) {
      size += r.serializer(delegate).size(r, o);
    }
    return size;
  }

  @Override
  public void write(Registry r, DataOutputStream out, T o) throws IOException, SerializationException {
    final Serializer<Boolean> sBool = r.serializer(Boolean.class);
    sBool.write(r, out, o != null);
    if (o != null) {
      r.serializer(delegate).write(r, out, o);
    }
  }

  @Override
  public T read(Registry r, DataInputStream in) throws IOException {
    final Serializer<Boolean> sBool = r.serializer(Boolean.class);
    if (sBool.read(r, in)) {
      return r.serializer(delegate).read(r, in);
    }
    return null;
  }

}
