package plus.bio.ser;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.SortedSet;

import plus.bio.ser.api.MigrationStrategy;
import plus.bio.ser.api.Registry;

/**
 * Static utilities for dealing with data migration.
 *
 * @author Tom Davis
 */
public final class Migrations {

  /**
   * Serially apply migrations in <code>strats</code> and return the final data stream
   *
   * @param strats Migrations to apply
   * @param r Registry used in {@link MigrationStrategy#read(Registry, DataInputStream)}
   * @param in Initial input passed to first migration
   * @return Final, migrated data input
   *
   * @throws IOException If any error occurred during reading/writing new data
   */
  public static DataInputStream applyAll(SortedSet<MigrationStrategy> strats, Registry r,
      DataInputStream in) throws IOException {
    DataInputStream newIn = in;
    for (MigrationStrategy strat : strats) {
      newIn = strat.read(r, newIn);
    }
    return newIn;
  }

}
