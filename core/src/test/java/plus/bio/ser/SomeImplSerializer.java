package plus.bio.ser;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.SerializationException;
import plus.bio.ser.api.Serializer;
import plus.bio.ser.serializers.IntegerSerializer;

public class SomeImplSerializer implements Serializer<SomeImpl> {

  Serializer<Integer> delegate = new IntegerSerializer();

  @Override
  public int size(Registry r, SomeImpl o) throws MissingSerializerException {
    return delegate.size(r, o.getValue());
  }

  @Override
  public void write(Registry r, DataOutputStream out, SomeImpl o)
      throws IOException, SerializationException {
    delegate.write(r, out, o.getValue());
  }

  @Override
  public SomeImpl read(Registry r, DataInputStream in) throws IOException {
    return new SomeImpl(delegate.read(r, in));
  }
}
