package plus.bio.ser;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.emptyArray;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;
import plus.bio.ser.provider.ArrayListProvider;
import plus.bio.ser.provider.HashMapProvider;
import plus.bio.ser.serializers.ArraySerializer;
import plus.bio.ser.serializers.CollectionSerializer;
import plus.bio.ser.serializers.EnumSerializer;
import plus.bio.ser.serializers.InterfaceSerializer;
import plus.bio.ser.serializers.MapSerializer;
import plus.bio.ser.serializers.ThrowableSerializer;

public class SerializerTest {

  ByteArrayOutputStream outArray;
  DataOutputStream out;
  Registry r;

  @Before
  public void setUp() {
    outArray = new ByteArrayOutputStream();
    out = new DataOutputStream(outArray);
    r = new DefaultRegistry();
  }

  @Test
  public void testLongSerializer() throws IOException {
    final long val = Long.MAX_VALUE;
    final int expectedSize = 8;
    Serializer<Long> ser = r.serializer(Long.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testDateSerializer() throws IOException {
    final Date val = new Date();
    final int expectedSize = 8;
    Serializer<Date> ser = r.serializer(Date.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testBooleanSerializer() throws IOException {
    final boolean val = true;
    final int expectedSize = 1;
    Serializer<Boolean> ser = r.serializer(Boolean.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testCharacterSerializer() throws IOException {
    final char val = 'c';
    final int expectedSize = 2;
    Serializer<Character> ser = r.serializer(Character.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testByteSerializer() throws IOException {
    final byte val = 4;
    final int expectedSize = 1;
    Serializer<Byte> ser = r.serializer(Byte.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testShortSerializer() throws IOException {
    final short val = 42;
    final int expectedSize = 2;
    Serializer<Short> ser = r.serializer(Short.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testIntegerSerializer() throws IOException {
    final int val = 42;
    final int expectedSize = 4;
    Serializer<Integer> ser = r.serializer(Integer.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testFloatSerializer() throws IOException {
    final float val = 42.1f;
    final int expectedSize = 4;
    Serializer<Float> ser = r.serializer(Float.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testDoubleSerializer() throws IOException {
    final double val = 42.1;
    final int expectedSize = 8;
    Serializer<Double> ser = r.serializer(Double.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testStringSerializer() throws IOException {
    final String val = "foo";
    final int expectedSize = 5;
    Serializer<String> ser = r.serializer(String.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);

    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testUUIDSerializer() throws IOException {
    final UUID val = UUID.randomUUID();
    final int expectedSize = 38;
    Serializer<UUID> ser = r.serializer(UUID.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);

    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testEnumSerializer() throws IOException {
    final TestEnum val = TestEnum.TWO;
    final int expectedSize = 43;
    Serializer<TestEnum> ser = new EnumSerializer<TestEnum>();

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);

    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testListSerializer() throws IOException {
    final List<String> val = ImmutableList.of("foo", "bar", "baz");
    // List contents + type + size
    final int expectedSize = 15 + 4 + 4;
    Serializer<List<String>> ser = CollectionSerializer.create(new ArrayListProvider<String>(), String.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testMapSerializer() throws IOException {
    final Map<String, Long> val = ImmutableMap.of("foo", 1L, "bar", 2L);
    // Strings + Longs + key-type + val-type + size
    final int expectedSize = 10 + 16 + 4 + 4 + 4;
    Serializer<Map<String, Long>> ser = MapSerializer.create(new HashMapProvider<String, Long>(), String.class, Long.class);

    assertThat(ser.size(r, val), is(expectedSize));

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testArraySerializerWithUnboxed() throws IOException {
    final String[] val = new String[]{"foo"};
    final Serializer<String[]> ser = new ArraySerializer<>();
    final int expectedSize = ser.size(r, val);

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    assertThat(ser.read(r, in()), is(val));
  }

  @Test
  public void testThrowableSerializerWithoutCause() throws IOException {
    Throwable val;
    try {
      throw new RuntimeException("boom");
    } catch (RuntimeException e) {
      val = e;
    }
    final Serializer<Throwable> ser = new ThrowableSerializer();
    final int expectedSize = ser.size(r, val);

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    final Throwable got = ser.read(r, in());
    assertThat(got, instanceOf(RuntimeException.class));
    assertThat(got.getMessage(), is(val.getMessage()));
    assertThat(got.getStackTrace(), is(val.getStackTrace()));
  }

  @Test
  public void testThrowableSerializerWithCauseWithoutStackTrace() throws IOException {
    final Throwable cause = new RuntimeException("cause");
    final Throwable val = new RuntimeException("boom", cause);
    val.setStackTrace(new StackTraceElement[]{});
    final Serializer<Throwable> ser = new ThrowableSerializer();
    final int expectedSize = ser.size(r, val);

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    final Throwable got = ser.read(r, in());
    assertThat(got, instanceOf(RuntimeException.class));
    assertThat(got.getMessage(), is(val.getMessage()));
    assertThat(got.getCause(), not(nullValue()));
    assertThat(got.getCause().getMessage(), is(cause.getMessage()));
    assertThat(got.getCause().getStackTrace(), is(cause.getStackTrace()));
    assertThat(got.getCause().getCause(), is(nullValue()));
    assertThat(got.getStackTrace(), emptyArray());
  }

  @Test
  public void testInterfaceSerializer() throws IOException {
    r.register(SomeImpl.class, new SomeImplSerializer());

    final Serializer<SomeInterface> ser = new InterfaceSerializer<>();
    final SomeImpl val = new SomeImpl(42);

    final int expectedSize = ser.size(r, val);

    ser.write(r, out, val);
    assertThat(outArray.size(), is(expectedSize));

    final SomeInterface got = ser.read(r, in());
    assertThat(got, instanceOf(SomeImpl.class));
    assertThat(got.getValue(), is(val.getValue()));
  }

  private DataInputStream in() {
    ByteArrayInputStream in = new ByteArrayInputStream(outArray.toByteArray());
    return new DataInputStream(in);
  }

  public enum TestEnum {
    ONE, TWO;
  }

}
