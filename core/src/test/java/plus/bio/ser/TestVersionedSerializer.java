package plus.bio.ser;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;
import plus.bio.ser.api.VersionedSerializer;

class TestVersionedSerializer implements Serializer<DefaultRegistryTest>, VersionedSerializer {

  @Override
  public int size(Registry r, DefaultRegistryTest o) throws MissingSerializerException {
    return 0;
  }

  @Override
  public void write(Registry r, DataOutputStream out, DefaultRegistryTest o) throws IOException {
  }

  @Override
  public DefaultRegistryTest read(Registry r, DataInputStream in) throws IOException {
    return null;
  }

  @Override
  public int version() {
    return 5;
  }
}
