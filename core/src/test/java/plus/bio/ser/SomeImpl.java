package plus.bio.ser;

public class SomeImpl implements SomeInterface {

  int value;

  public SomeImpl(int i) {
    value = i;
  }

  @Override
  public int getValue() {
    return value;
  }

  @Override
  public void setValue(int value) {
    this.value = value;
  }
}
