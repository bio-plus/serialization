package plus.bio.ser;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import plus.bio.ser.api.MigrationStrategy;
import plus.bio.ser.api.MissingSerializerException;
import plus.bio.ser.api.NoMigrationPathException;
import plus.bio.ser.api.Registry;
import plus.bio.ser.api.Serializer;
import plus.bio.ser.serializers.LongSerializer;

public class DefaultRegistryTest {

  private Registry r;

  @Before
  public void setUp() {
    r = new DefaultRegistry();
  }

  @Test(expected = MissingSerializerException.class)
  public void testMissingSerializerByClass() throws IOException {
    r.serializer(SerializerTest.class);
  }

  @Test(expected = MissingSerializerException.class)
  public void testMissingSerializerById() throws IOException {
    r.serializer(-1);
  }

  @Test
  public void testPresentSerializerByClass() throws IOException {
    assertThat(r.serializer(Long.class), instanceOf(LongSerializer.class));
  }

  @Test
  public void testPresentSerializerById() throws IOException {
    r.serializer(r.id(Long.class));
  }

  @Test
  public void testRegistrationAndRetrieval() throws IOException {
    r.register(DefaultRegistryTest.class, serializer);

    assertThat(r.serializer(DefaultRegistryTest.class), is(serializer));
  }

  @Test
  public void testMigrationWithMultipleSteps() throws IOException, NoMigrationPathException {
    r.register(DefaultRegistryTest.class, serializer);

    r.addMigration(DefaultRegistryTest.class, oneToTwo);
    r.addMigration(DefaultRegistryTest.class, twoToThree);

    SortedSet<MigrationStrategy> expected = new TreeSet<>();
    expected.add(oneToTwo);
    expected.add(twoToThree);

    assertThat(r.migrations(DefaultRegistryTest.class, 1, 3), is(expected));
  }

  @Test
  public void testMigrationWithOneStep() throws IOException, NoMigrationPathException {
    r.register(DefaultRegistryTest.class, serializer);

    r.addMigration(DefaultRegistryTest.class, oneToTwo);
    r.addMigration(DefaultRegistryTest.class, twoToThree);
    r.addMigration(DefaultRegistryTest.class, oneToThree);

    SortedSet<MigrationStrategy> expected = new TreeSet<>();
    expected.add(oneToThree);

    assertThat(r.migrations(DefaultRegistryTest.class, 1, 3), is(expected));
  }

  @Test(expected = NoMigrationPathException.class)
  public void testMigrationWithNoPath() throws IOException, NoMigrationPathException {
    r.register(DefaultRegistryTest.class, serializer);

    r.addMigration(DefaultRegistryTest.class, oneToTwo);
    r.addMigration(DefaultRegistryTest.class, twoToThree);
    r.addMigration(DefaultRegistryTest.class, oneToThree);

    r.migrations(DefaultRegistryTest.class, 1, 5);
  }

  @Test
  public void testItUsesVersioningWrapper() throws IOException, NoMigrationPathException {
    r.register(DefaultRegistryTest.class, new TestVersionedSerializer());

    assertThat(r.serializer(DefaultRegistryTest.class), instanceOf(VersioningWrapper.class));
  }

  @Test
  public void testInterfaceHandling() throws IOException {
    final Serializer<SomeInterface> s = r.serializer(SomeInterface.class);
    assertThat(s, not(nullValue()));
  }

  private static final MigrationStrategy oneToTwo = new MigrationStrategy() {

    @Override
    public int fromVersion() {
      return 1;
    }

    @Override
    public int toVersion() {
      return 2;
    }

    @Override
    public DataInputStream read(Registry r, DataInputStream in) throws IOException {
      return in;
    }
  };

  private static final MigrationStrategy twoToThree = new MigrationStrategy() {

    @Override
    public int fromVersion() {
      return 2;
    }

    @Override
    public int toVersion() {
      return 3;
    }

    @Override
    public DataInputStream read(Registry r, DataInputStream in) throws IOException {
      return in;
    }
  };

  private static final MigrationStrategy oneToThree = new MigrationStrategy() {

    @Override
    public int fromVersion() {
      return 1;
    }

    @Override
    public int toVersion() {
      return 3;
    }

    @Override
    public DataInputStream read(Registry r, DataInputStream in) throws IOException {
      return in;
    }
  };

  private static final Serializer<DefaultRegistryTest> serializer =
      new Serializer<DefaultRegistryTest>() {

        @Override
        public int size(Registry r, DefaultRegistryTest o) throws MissingSerializerException {
          return 0;
        }

        @Override
        public void write(Registry r, DataOutputStream out, DefaultRegistryTest o)
            throws IOException {}

        @Override
        public DefaultRegistryTest read(Registry r, DataInputStream in) throws IOException {
          return null;
        }
      };
}
