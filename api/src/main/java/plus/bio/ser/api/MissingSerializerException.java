package plus.bio.ser.api;

import java.io.IOException;

/**
 * No serializer could be found for {@code klass}.
 */
public class MissingSerializerException extends IOException {
  private static final long serialVersionUID = -1696424385010546691L;

  public final Class<?> klass;
  public final int id;

  public MissingSerializerException(int id) {
    klass = null;
    this.id = id;
  }

  public MissingSerializerException(Class<?> c) {
    klass = c;
    this.id = 0;
  }

  @Override
  public String toString() {
    return String.format("No serializer for class %s", klass != null ? klass.getName() : id);
  }
}
