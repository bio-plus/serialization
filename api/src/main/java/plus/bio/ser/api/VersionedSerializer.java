package plus.bio.ser.api;

/**
 * Optional interface that tells {@link plus.bio.ser.DefaultRegistry} to wrap a serializer in {@link
 * plus.bio.ser.VersioningWrapper}.
 */
public interface VersionedSerializer {
  /**
   * Current object version the serializer can produce/consume.
   * <p>
   * Versions should be incremented when a object's serialized fields change. Bumping the version
   * generally requires writing and registering an additional {@link MigrationStrategy}.
   *
   * @return Current version
   */
  int version();
}
