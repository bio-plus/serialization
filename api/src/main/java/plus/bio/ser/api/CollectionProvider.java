package plus.bio.ser.api;

import java.util.Collection;

/**
 * Used by {@link plus.bio.ser.serializers.CollectionSerializer} to implement serializers for
 * arbitrary collections.
 */
public interface CollectionProvider<C, T extends Collection<C>> {
  /**
   * Creates an empty collection of type {@code T} capable of holding {@code size} number of items.
   *
   * @param size Number of items in collection
   * @return Empty collection
   */
  T apply(int size);
}
