package plus.bio.ser.api;

import java.util.Map;

/**
 * Used by {@link plus.bio.ser.serializers.MapSerializer} to implement serializers for arbitrary
 * mappings.
 */
public interface MapProvider<K, V> {
  /**
   * Creates an empty map of type {@code <K,V>} capable of holding {@code size} number of items.
   *
   * @param size Number of items in collection
   * @return Empty map
   */
  Map<K, V> apply(int size);
}
