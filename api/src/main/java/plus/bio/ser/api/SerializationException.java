package plus.bio.ser.api;

import java.io.IOException;

/**
 * IO-type exception that serializers can throw to provide more context when serialization fails.
 *
 * While defined specifically as being thrown by {@link Serializer#write}, as an {@code
 * IOException} sub-class it is an optional exception; serializers may still throw a more generic
 * {@code IOException}.
 *
 * @author Tom Davis
 */
public class SerializationException extends IOException {
  private static final long serialVersionUID = 1L;

  public final String field;
  public final Class<?> serializingClass;
  public final String message;

  public SerializationException(String message) {
    this(message, null, null);
  }

  public SerializationException(String message, Class<?> serializingClass) {
    this(message, serializingClass, null);
  }

  public SerializationException(String message, Class<?> serializingClass, String field) {
    this.message = message;
    this.serializingClass = serializingClass;
    this.field = field;
  }

  public SerializationException(String string, Exception e) {
    super(string, e);
    this.message = string;
    this.serializingClass = null;
    this.field = null;
  }

  public SerializationException(Class<?> serializingClass, Exception e) {
    super(e);
    this.message = e.getMessage();
    this.serializingClass = serializingClass;
    this.field = null;
  }

  @Override
  public String toString() {
    if (field != null) {
      return String.format("Failed serializing %s#%s: %s", serializingClass.getName(), field, message);
    }
    else if (serializingClass != null) {
      return String.format("Failed serializing %s: %s", serializingClass.getName(), message);
    }
    return  String.format("Failed serializing: %s", message);
  }

}
