package plus.bio.ser.api;

import java.io.IOException;

/**
 * Could not find a migration between two versions.
 */
public class NoMigrationPathException extends IOException {
  private static final long serialVersionUID = 1L;
}
