package plus.bio.ser.api;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Abstract class for implementing migrations from old data storage formats to newer ones.
 */
public abstract class MigrationStrategy implements Comparable<MigrationStrategy> {
  /**
   * Exact version migrating <i>from</i>
   *
   * @return Version this migrates from
   */
  public abstract int fromVersion();

  /**
   * Exact version migrating <i>tom</i>
   *
   * Migrations can span multiple versions; from 1 to 2 but also 2 to 5, etc.
   *
   * @return Version this migrates to
   */
  public abstract int toVersion();

  /**
   * Read data stored in {@code fromVersion()}, returning that same data
   *
   * @param r Registry of serializers
   * @param in Input data
   * @return New input data
   *
   * @throws IOException Problem reading from input stream
   */
  public abstract DataInputStream read(Registry r, DataInputStream in) throws IOException;

  /**
   * @param o2 Strategy compared
   * @return Comparison optimized for minimum number of steps to migrate between two versions
   */
  @Override
  public int compareTo(MigrationStrategy o2) {
    int result = new Integer(fromVersion()).compareTo(o2.fromVersion());
    if (result == 0) {
      return new Integer(toVersion()).compareTo(o2.toVersion()) * -1;
    }
    return result;
  }

}
