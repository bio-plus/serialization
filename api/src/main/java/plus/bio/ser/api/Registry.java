package plus.bio.ser.api;

import java.util.SortedSet;

/**
 * Registry of serializers and migrations for different types.
 *
 * Prefer the {@link plus.bio.ser.DefaultRegistry} implementation of this interface over creating
 * your own.
 */
public interface Registry {
  /**
   * Register {@code s} as a serializer for objects of type {@code c}.
   *
   * Registration is stored as a standard Map; last registration for a {@code c} wins.
   *
   * @param c Type the serializer works with
   * @param s Instance of serializer
   */
  <T> void register(Class<T> c, Serializer<T> s);

  /**
   * Add {@code strategy} to the migrations available for {@code c}
   *
   * @param c Type who's data can be migrated
   * @param strategy Strategy to migrate the data
   */
  <T> void addMigration(Class<T> c, MigrationStrategy strategy);

  /**
   * Retrieve the serializer for type {@code c}
   *
   * @param c Type to serializer
   * @return Serializer
   *
   * @throws MissingSerializerException No serializer registered for {@code c}
   */
  <T> Serializer<T> serializer(Class<T> c) throws MissingSerializerException;

  /**
   * Retrieve the serializer for type with unique ID {@code id}
   *
   * @param id Unique ID of the type
   * @return Serializer for type with {@code id}
   *
   * @throws MissingSerializerException No serializer registered for the ID
   */
  <T> Serializer<T> serializer(int id) throws MissingSerializerException;

  /**
   * Given a type to serialize, return a globally-unique ID that can identify it during
   * deserialization.
   *
   * @param c Type to deserialize
   * @return Its unique ID
   */
  <T> int id(Class<T> c);

  /**
   * Construct a set of migrations to take an object of type {@code c} from {@code fromVersion} to
   * {@code toVersion}.
   *
   * @param c Type to migration
   * @param fromVersion Version migrating from
   * @param toVersion Version migrating to
   * @return Ordered set of migrations to apply to the data
   *
   * @throws NoMigrationPathException No suitable path from {@code fromVersion} to {@code toVersion}
   * could be found.
   */
  <T> SortedSet<MigrationStrategy> migrations(Class<T> c, int fromVersion, int toVersion) throws NoMigrationPathException;
}
