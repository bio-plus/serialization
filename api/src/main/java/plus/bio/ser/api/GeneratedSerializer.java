package plus.bio.ser.api;

/**
 * Implemented by serializers created via {@link plus.bio.ser.anno.AutoSerializer}.
 */
public interface GeneratedSerializer<T> extends Serializer<T> {
  /**
   * Registers this serializer and any created for its fields with {@code r}.
   *
   * @param r Registry to register with
   */
  void registerWith(Registry r);
}
