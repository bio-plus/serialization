=========================
 Bio+ Data Serialization
=========================

This library provides a mechanism for serializing and deserializing objects as binary data, used by the scoring_ library and emma_.

See the detailed javadoc_ or generate it yourself with ``mvn javadoc:aggregate``.


.. _scoring: https://gitlab.com/bio-plus/scoring
.. _emma: https://gitlab.com/bio-plus/emma
.. _javadoc: https://bioplus.io/docs/serialization
